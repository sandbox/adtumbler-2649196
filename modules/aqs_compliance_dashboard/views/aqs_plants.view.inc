<?php

$view = new view();
$view->name = 'aqs_plants';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'aqs_plants';
$view->human_name = 'AQS Plants';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['columns'] = array(
  'id' => 'id',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 'striped',
  'condensed' => 'condensed',
  'bordered' => 0,
  'hover' => 0,
);
/* Field: AQS Plant: Plant ID */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'aqs_plants';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['exclude'] = TRUE;
/* Field: AQS Plant: Plant Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'aqs_plants';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = 'Plant';
/* Field: Field: County */
$handler->display->display_options['fields']['field_county']['id'] = 'field_county';
$handler->display->display_options['fields']['field_county']['table'] = 'field_data_field_county';
$handler->display->display_options['fields']['field_county']['field'] = 'field_county';
$handler->display->display_options['fields']['field_county']['settings'] = array(
  'link' => 0,
);
$handler->display->display_options['fields']['field_county']['delta_offset'] = '0';
/* Filter criterion: AQS Plant: Plant Name */
$handler->display->display_options['filters']['name']['id'] = 'name';
$handler->display->display_options['filters']['name']['table'] = 'aqs_plants';
$handler->display->display_options['filters']['name']['field'] = 'name';
$handler->display->display_options['filters']['name']['operator'] = 'contains';
$handler->display->display_options['filters']['name']['group'] = 1;
$handler->display->display_options['filters']['name']['exposed'] = TRUE;
$handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['label'] = 'Filter by Plant';
$handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
$handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  4 => 0,
  6 => 0,
  5 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['name']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['name']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['name']['expose']['autocomplete_field'] = 'name';
$handler->display->display_options['filters']['name']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['name']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['name']['expose']['autocomplete_dependent'] = 0;
