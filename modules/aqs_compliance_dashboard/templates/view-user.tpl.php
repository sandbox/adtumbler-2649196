<?php
/**
 * @file
 * Template file for aqs user pages
 *
 * Available custom variables:
 * -
 */
?>

<p><?php print render($email); ?></p>
<?php if (!empty($office)): ?>
  <p><strong>Office:</strong> <?php print render($office); ?></p>
<?php endif; ?>
<?php print render($role); ?>
<?php print render($editlink); ?>
