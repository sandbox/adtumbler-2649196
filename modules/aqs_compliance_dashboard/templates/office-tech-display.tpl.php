<?php
/**
 * @file
 * Template file for aqs compliance
 *
 * Available custom variables:
 * - $label: Office Name
 * - $address: physical address
 * - $users: list of users belonging to office
 * - $adduser: link to add a user
 * - $addtank: link to add a tank
 * - $tanks: list of tanks belonging to office
 */
?>

<p><?php print render($address); ?></p>
<p>Users:</p>
<?php print render($users); ?>
<?php print render($adduser); ?>
<p>Tanks:</p>
<?php print render($tanks); ?>
<?php print render($addtank); ?>
