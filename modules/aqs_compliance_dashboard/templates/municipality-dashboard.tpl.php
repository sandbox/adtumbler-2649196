<?php
/**
 * @file
 * Template file for aqs compliance
 *
 * Available custom variables:
 * - $view
 */
?>
  <!-- AQS Compliance -->

<?php if (empty($view)): ?>
  <div class="compliance-container row">
    <div class="col-xs-6">
      <a class="section" href="/compliance/summary"><span class="glyphicon glyphicon-list">&nbsp;</span>
        <h4>Summary Report</h4>
      </a>
    </div>

    <div class="col-xs-6">
      <a class="section" href="/compliance/detail"><span class="glyphicon glyphicon-list-alt">&nbsp;</span>
        <h4>Detailed Report</h4>
      </a>
    </div>
  </div>
<?php else: ?>
  <div class="muni-box">
    <?php print render($view); ?>
  </div>
<?php endif; ?>

