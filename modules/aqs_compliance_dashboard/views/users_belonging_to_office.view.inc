<?php

$view = new view();
$view->name = 'users_belonging_to_office';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'redhen_contact';
$view->human_name = 'Users belonging to Office';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: Contact: Relation: Organizational affiliation (redhen_contact → redhen_org) */
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_org']['id'] = 'relation_redhen_affiliation_redhen_org';
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_org']['table'] = 'redhen_contact';
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_org']['field'] = 'relation_redhen_affiliation_redhen_org';
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_org']['r_index'] = '-1';
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_org']['entity_deduplication_left'] = 0;
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_org']['entity_deduplication_right'] = 0;
/* Field: Contact: Full name */
$handler->display->display_options['fields']['full_name']['id'] = 'full_name';
$handler->display->display_options['fields']['full_name']['table'] = 'views_entity_redhen_contact';
$handler->display->display_options['fields']['full_name']['field'] = 'full_name';
$handler->display->display_options['fields']['full_name']['label'] = '';
$handler->display->display_options['fields']['full_name']['exclude'] = TRUE;
$handler->display->display_options['fields']['full_name']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['full_name']['link_to_entity'] = 0;
/* Field: Contact: Default email */
$handler->display->display_options['fields']['email']['id'] = 'email';
$handler->display->display_options['fields']['email']['table'] = 'views_entity_redhen_contact';
$handler->display->display_options['fields']['email']['field'] = 'email';
$handler->display->display_options['fields']['email']['label'] = '';
$handler->display->display_options['fields']['email']['exclude'] = TRUE;
$handler->display->display_options['fields']['email']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['email']['link_to_entity'] = 0;
/* Field: Field: Phone */
$handler->display->display_options['fields']['field_phone']['id'] = 'field_phone';
$handler->display->display_options['fields']['field_phone']['table'] = 'field_data_field_phone';
$handler->display->display_options['fields']['field_phone']['field'] = 'field_phone';
$handler->display->display_options['fields']['field_phone']['label'] = '';
$handler->display->display_options['fields']['field_phone']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_phone']['element_label_colon'] = FALSE;
/* Field: Contact: Contact ID */
$handler->display->display_options['fields']['contact_id']['id'] = 'contact_id';
$handler->display->display_options['fields']['contact_id']['table'] = 'redhen_contact';
$handler->display->display_options['fields']['contact_id']['field'] = 'contact_id';
$handler->display->display_options['fields']['contact_id']['label'] = '';
$handler->display->display_options['fields']['contact_id']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['contact_id']['alter']['text'] = '<h4>[full_name]</h4>
[email]
[field_phone]
<a href="/compliance/offices/!1/edituser/[contact_id]" class="btn btn-link btn-sm"><span class="glyphicon glyphicon-edit"></span> edit</a>';
$handler->display->display_options['fields']['contact_id']['alter']['absolute'] = TRUE;
$handler->display->display_options['fields']['contact_id']['element_label_colon'] = FALSE;
/* Contextual filter: Organization: Organization ID */
$handler->display->display_options['arguments']['org_id']['id'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['arguments']['org_id']['field'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['relationship'] = 'relation_redhen_affiliation_redhen_org';
$handler->display->display_options['arguments']['org_id']['default_action'] = 'not found';
$handler->display->display_options['arguments']['org_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['org_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['org_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['org_id']['summary_options']['items_per_page'] = '25';

/* Display: Embed */
$handler = $view->new_display('embed', 'Embed', 'embed_1');
