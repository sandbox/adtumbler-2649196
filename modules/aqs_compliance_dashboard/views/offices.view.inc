<?php

$view = new view();
$view->name = 'offices';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'redhen_org';
$view->human_name = 'Offices';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Offices';
$handler->display->display_options['css_class'] = 'table-responsive';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Clear';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['style_options']['columns'] = array(
  'org_id' => 'org_id',
  'label' => 'label',
  'field_address_locality' => 'field_address_locality',
  'name' => 'name',
  'field_address_administrative_area' => 'field_address_administrative_area',
  'nothing' => 'nothing',
);
$handler->display->display_options['style_options']['default'] = 'label';
$handler->display->display_options['style_options']['info'] = array(
  'org_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'label' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_address_locality' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_address_administrative_area' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'nothing' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 'striped',
  'bordered' => 0,
  'hover' => 0,
  'condensed' => 0,
);
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = '<h4>No offices found</h4>
<a class="btn btn-primary" href="offices/add">Create new office</a>';
$handler->display->display_options['empty']['area']['format'] = 'full_html';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_county_target_id']['id'] = 'field_county_target_id';
$handler->display->display_options['relationships']['field_county_target_id']['table'] = 'field_data_field_county';
$handler->display->display_options['relationships']['field_county_target_id']['field'] = 'field_county_target_id';
$handler->display->display_options['relationships']['field_county_target_id']['label'] = 'AQS County entity';
/* Field: Organization: Organization ID */
$handler->display->display_options['fields']['org_id']['id'] = 'org_id';
$handler->display->display_options['fields']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['fields']['org_id']['field'] = 'org_id';
$handler->display->display_options['fields']['org_id']['label'] = '';
$handler->display->display_options['fields']['org_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['org_id']['alter']['path'] = 'compliance/offices/adduser/[org_id]';
$handler->display->display_options['fields']['org_id']['alter']['link_class'] = 'btn btn-primary';
$handler->display->display_options['fields']['org_id']['element_label_colon'] = FALSE;
/* Field: Organization: Organization Name */
$handler->display->display_options['fields']['label']['id'] = 'label';
$handler->display->display_options['fields']['label']['table'] = 'redhen_org';
$handler->display->display_options['fields']['label']['field'] = 'label';
$handler->display->display_options['fields']['label']['label'] = 'Office Name';
$handler->display->display_options['fields']['label']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['label']['alter']['path'] = 'compliance/offices/[org_id]';
/* Field: Organization: Address - Locality (i.e. City) */
$handler->display->display_options['fields']['field_address_locality']['id'] = 'field_address_locality';
$handler->display->display_options['fields']['field_address_locality']['table'] = 'field_data_field_address';
$handler->display->display_options['fields']['field_address_locality']['field'] = 'field_address_locality';
$handler->display->display_options['fields']['field_address_locality']['label'] = 'City';
/* Field: AQS County: County Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'aqs_counties';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'field_county_target_id';
$handler->display->display_options['fields']['name']['label'] = 'County';
/* Field: Organization: Address - Administrative area (i.e. State / Province) */
$handler->display->display_options['fields']['field_address_administrative_area']['id'] = 'field_address_administrative_area';
$handler->display->display_options['fields']['field_address_administrative_area']['table'] = 'field_data_field_address';
$handler->display->display_options['fields']['field_address_administrative_area']['field'] = 'field_address_administrative_area';
$handler->display->display_options['fields']['field_address_administrative_area']['label'] = 'State';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="/compliance/offices/[org_id]/edit" class="btn btn-link btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit Office</a>';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
/* Filter criterion: Organization: Organization Name */
$handler->display->display_options['filters']['label']['id'] = 'label';
$handler->display->display_options['filters']['label']['table'] = 'redhen_org';
$handler->display->display_options['filters']['label']['field'] = 'label';
$handler->display->display_options['filters']['label']['operator'] = 'word';
$handler->display->display_options['filters']['label']['group'] = 1;
$handler->display->display_options['filters']['label']['exposed'] = TRUE;
$handler->display->display_options['filters']['label']['expose']['operator_id'] = 'label_op';
$handler->display->display_options['filters']['label']['expose']['label'] = 'Office Name';
$handler->display->display_options['filters']['label']['expose']['operator'] = 'label_op';
$handler->display->display_options['filters']['label']['expose']['identifier'] = 'label';
$handler->display->display_options['filters']['label']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  4 => 0,
  6 => 0,
  5 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['label']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['label']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['label']['expose']['autocomplete_field'] = 'label';
$handler->display->display_options['filters']['label']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['label']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['label']['expose']['autocomplete_dependent'] = 0;
/* Filter criterion: Organization: Address - Locality (i.e. City) */
$handler->display->display_options['filters']['field_address_locality']['id'] = 'field_address_locality';
$handler->display->display_options['filters']['field_address_locality']['table'] = 'field_data_field_address';
$handler->display->display_options['filters']['field_address_locality']['field'] = 'field_address_locality';
$handler->display->display_options['filters']['field_address_locality']['operator'] = 'contains';
$handler->display->display_options['filters']['field_address_locality']['group'] = 1;
$handler->display->display_options['filters']['field_address_locality']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_address_locality']['expose']['operator_id'] = 'field_address_locality_op';
$handler->display->display_options['filters']['field_address_locality']['expose']['label'] = 'City';
$handler->display->display_options['filters']['field_address_locality']['expose']['operator'] = 'field_address_locality_op';
$handler->display->display_options['filters']['field_address_locality']['expose']['identifier'] = 'field_address_locality';
$handler->display->display_options['filters']['field_address_locality']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  4 => 0,
  5 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['field_address_locality']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['field_address_locality']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['field_address_locality']['expose']['autocomplete_field'] = 'field_address_locality';
$handler->display->display_options['filters']['field_address_locality']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['field_address_locality']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['field_address_locality']['expose']['autocomplete_dependent'] = 0;
/* Filter criterion: AQS County: County Name */
$handler->display->display_options['filters']['name']['id'] = 'name';
$handler->display->display_options['filters']['name']['table'] = 'aqs_counties';
$handler->display->display_options['filters']['name']['field'] = 'name';
$handler->display->display_options['filters']['name']['relationship'] = 'field_county_target_id';
$handler->display->display_options['filters']['name']['group'] = 1;
$handler->display->display_options['filters']['name']['exposed'] = TRUE;
$handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['label'] = 'County';
$handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
$handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  4 => 0,
  5 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['name']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['name']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['name']['expose']['autocomplete_field'] = 'name';
$handler->display->display_options['filters']['name']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['name']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['name']['expose']['autocomplete_dependent'] = 0;
/* Filter criterion: Organization: Address - Administrative area (i.e. State / Province) */
$handler->display->display_options['filters']['field_address_administrative_area']['id'] = 'field_address_administrative_area';
$handler->display->display_options['filters']['field_address_administrative_area']['table'] = 'field_data_field_address';
$handler->display->display_options['filters']['field_address_administrative_area']['field'] = 'field_address_administrative_area';
$handler->display->display_options['filters']['field_address_administrative_area']['group'] = 1;
$handler->display->display_options['filters']['field_address_administrative_area']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['operator_id'] = 'field_address_administrative_area_op';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['label'] = 'State';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['operator'] = 'field_address_administrative_area_op';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['identifier'] = 'field_address_administrative_area';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  4 => 0,
  5 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['autocomplete_field'] = 'field_address_administrative_area';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['autocomplete_dependent'] = 0;
/* Filter criterion: Organization: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'redhen_org';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dental_office' => 'dental_office',
);

/* Display: by_city */
$handler = $view->new_display('embed', 'by_city', 'by_city');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['style_options']['columns'] = array(
  'org_id' => 'org_id',
  'field_address_locality' => 'field_address_locality',
  'label' => 'label',
  'nothing' => 'nothing',
);
$handler->display->display_options['style_options']['default'] = 'field_address_locality';
$handler->display->display_options['style_options']['info'] = array(
  'org_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_address_locality' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'label' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'nothing' => array(
    'align' => 'views-align-right',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 'striped',
  'condensed' => 'condensed',
  'bordered' => 0,
  'hover' => 0,
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Organization: Organization ID */
$handler->display->display_options['fields']['org_id']['id'] = 'org_id';
$handler->display->display_options['fields']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['fields']['org_id']['field'] = 'org_id';
$handler->display->display_options['fields']['org_id']['label'] = '';
$handler->display->display_options['fields']['org_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['org_id']['alter']['path'] = 'compliance/offices/adduser/[org_id]';
$handler->display->display_options['fields']['org_id']['alter']['link_class'] = 'btn btn-primary';
$handler->display->display_options['fields']['org_id']['element_label_colon'] = FALSE;
/* Field: Organization: Address - Locality (i.e. City) */
$handler->display->display_options['fields']['field_address_locality']['id'] = 'field_address_locality';
$handler->display->display_options['fields']['field_address_locality']['table'] = 'field_data_field_address';
$handler->display->display_options['fields']['field_address_locality']['field'] = 'field_address_locality';
$handler->display->display_options['fields']['field_address_locality']['label'] = 'City';
/* Field: Organization: Organization Name */
$handler->display->display_options['fields']['label']['id'] = 'label';
$handler->display->display_options['fields']['label']['table'] = 'redhen_org';
$handler->display->display_options['fields']['label']['field'] = 'label';
$handler->display->display_options['fields']['label']['label'] = '';
$handler->display->display_options['fields']['label']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['label']['alter']['path'] = 'compliance/offices/[org_id]';
$handler->display->display_options['fields']['label']['element_label_colon'] = FALSE;
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="/compliance/offices/[org_id]" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit"></span> Select Office</a>';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Organization: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'redhen_org';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dental_office' => 'dental_office',
);
$handler->display->display_options['filters']['type']['group'] = 1;

/* Display: by_office */
$handler = $view->new_display('embed', 'by_office', 'by_office');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['style_options']['columns'] = array(
  'org_id' => 'org_id',
  'label' => 'label',
  'nothing' => 'nothing',
);
$handler->display->display_options['style_options']['default'] = 'label';
$handler->display->display_options['style_options']['info'] = array(
  'org_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'label' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'nothing' => array(
    'align' => 'views-align-right',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 'striped',
  'condensed' => 'condensed',
  'bordered' => 0,
  'hover' => 0,
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Organization: Organization ID */
$handler->display->display_options['fields']['org_id']['id'] = 'org_id';
$handler->display->display_options['fields']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['fields']['org_id']['field'] = 'org_id';
$handler->display->display_options['fields']['org_id']['label'] = '';
$handler->display->display_options['fields']['org_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['org_id']['alter']['path'] = 'compliance/offices/adduser/[org_id]';
$handler->display->display_options['fields']['org_id']['alter']['link_class'] = 'btn btn-primary';
$handler->display->display_options['fields']['org_id']['element_label_colon'] = FALSE;
/* Field: Organization: Organization Name */
$handler->display->display_options['fields']['label']['id'] = 'label';
$handler->display->display_options['fields']['label']['table'] = 'redhen_org';
$handler->display->display_options['fields']['label']['field'] = 'label';
$handler->display->display_options['fields']['label']['label'] = 'Office Name';
$handler->display->display_options['fields']['label']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['label']['alter']['path'] = 'compliance/offices/[org_id]';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="/compliance/offices/[org_id]" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit"></span> Select Office</a>';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Organization: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'redhen_org';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dental_office' => 'dental_office',
);
$handler->display->display_options['filters']['type']['group'] = 1;
