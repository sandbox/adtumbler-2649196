<?php
/**
 * @file
 * Template file for aqs compliance
 *
 * Available custom variables:
 * -
 */
?>
  <!-- AQS Compliance -->

  <div class="compliance-container row">
    <div class="col-xs-3" role="presentation">
      <a aria-controls="reports-admin" class="section" data-toggle="tab" href="#reports-admin" role="tab"><span class="glyphicon glyphicon-list-alt">&nbsp;</span>
        <h4>Reports</h4>
      </a>
    </div>

    <div class="col-xs-3" role="presentation">
      <a aria-controls="admin" class="section" data-toggle="tab" href="#admin" role="tab"><span class="glyphicon glyphicon-cog">&nbsp;</span>
        <h4>Administration</h4>
      </a>
    </div>

    <div class="col-xs-3" role="presentation">
      <a aria-controls="offices" class="section" data-toggle="tab" href="#offices" role="tab"><span class="glyphicon glyphicon-home">&nbsp;</span>

        <h4>Offices / Tanks</h4>
      </a>
    </div>

    <div class="col-xs-3">
      <a aria-controls="education-admin" class="section" data-toggle="tab" href="#education-admin" role="tab"><span class="glyphicon glyphicon-education">&nbsp;</span>

        <h4>Education</h4>
      </a>
    </div>
  </div>

  <div>
    <!-- Tab panes -->
    <div class="tab-content dialog-tabs">
      <div class="tab-pane fade active" id="reports-admin" role="tabpanel">
        <h2>Reports:</h2>
        <?php print render($cleaner_report); ?>
          <?php print render($inspection_report); ?>
            <?php print render($service_report); ?>

              <div class="report-display collapse" id="cleaner-reports">
                <?php print render($cleaner_report_view); ?>
              </div>
              <div class="report-display collapse" id="inspection-reports">
                <?php print render($inspection_report_view); ?>
              </div>
              <div class="report-display collapse" id="service-reports">
                <?php print render($service_report_view); ?>
              </div>
      </div>

      <div class="tab-pane fade" id="admin" role="tabpanel">
        <div>
          <?php print render($admin_areas); ?>
        </div>
      </div>

      <div class="tab-pane fade" id="offices" role="tabpanel">
        <?php print render($office_quick_search); ?>
        <?php print render($all_offices); ?>
        <?php print render($add_office); ?>
        <?php print render($tanks); ?>
        <?php print render($addtank); ?>
      </div>

      <div class="tab-pane fade" id="education-admin" role="tabpanel">
        <?php print render($courses); ?>
      </div>
    </div>
  </div>
