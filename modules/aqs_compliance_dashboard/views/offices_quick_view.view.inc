<?php

$view = new view();
$view->name = 'offices_quick_view';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'redhen_org';
$view->human_name = 'Offices Quick View';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Offices Quick View';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['link_display'] = 'custom_url';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access aqs clients';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Type to Select an Office';
$handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
$handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '3';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'views_bootstrap_grid_plugin_style';
$handler->display->display_options['style_options']['columns'] = '3';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Organization: Organization ID */
$handler->display->display_options['fields']['org_id']['id'] = 'org_id';
$handler->display->display_options['fields']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['fields']['org_id']['field'] = 'org_id';
$handler->display->display_options['fields']['org_id']['exclude'] = TRUE;
/* Field: Global: View */
$handler->display->display_options['fields']['view']['id'] = 'view';
$handler->display->display_options['fields']['view']['table'] = 'views';
$handler->display->display_options['fields']['view']['field'] = 'view';
$handler->display->display_options['fields']['view']['label'] = 'Tanks';
$handler->display->display_options['fields']['view']['exclude'] = TRUE;
$handler->display->display_options['fields']['view']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['view']['view'] = 'tanks_by_org';
$handler->display->display_options['fields']['view']['display'] = 'attachment_1';
$handler->display->display_options['fields']['view']['arguments'] = '[!org_id]';
/* Field: Organization: Organization Name */
$handler->display->display_options['fields']['label']['id'] = 'label';
$handler->display->display_options['fields']['label']['table'] = 'redhen_org';
$handler->display->display_options['fields']['label']['field'] = 'label';
$handler->display->display_options['fields']['label']['label'] = '';
$handler->display->display_options['fields']['label']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['label']['alter']['text'] = '<h4>[label]</h4>
<a href="/compliance/offices/[org_id]" class="btn btn-primary"><span class="glyphicon glyphicon-check"></span> Select Office</a>';
$handler->display->display_options['fields']['label']['alter']['absolute'] = TRUE;
$handler->display->display_options['fields']['label']['element_label_colon'] = FALSE;
/* Filter criterion: Organization: Organization Name */
$handler->display->display_options['filters']['label']['id'] = 'label';
$handler->display->display_options['filters']['label']['table'] = 'redhen_org';
$handler->display->display_options['filters']['label']['field'] = 'label';
$handler->display->display_options['filters']['label']['operator'] = 'contains';
$handler->display->display_options['filters']['label']['exposed'] = TRUE;
$handler->display->display_options['filters']['label']['expose']['operator_id'] = 'label_op';
$handler->display->display_options['filters']['label']['expose']['operator'] = 'label_op';
$handler->display->display_options['filters']['label']['expose']['identifier'] = 'label';
$handler->display->display_options['filters']['label']['expose']['required'] = TRUE;
$handler->display->display_options['filters']['label']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
);
$handler->display->display_options['filters']['label']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['label']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['label']['expose']['autocomplete_field'] = 'label';
$handler->display->display_options['filters']['label']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['label']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['label']['expose']['autocomplete_dependent'] = 0;
/* Filter criterion: Organization: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'redhen_org';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dental_office' => 'dental_office',
);
