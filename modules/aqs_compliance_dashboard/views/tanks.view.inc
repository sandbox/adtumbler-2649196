<?php

$view = new view();
$view->name = 'tanks';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'aqs_tanks';
$view->human_name = 'Tanks';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Tanks';
$handler->display->display_options['css_class'] = 'table-responsive';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Clear';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['columns'] = array(
  'serial_number' => 'serial_number',
  'label' => 'label',
  'capacity' => 'capacity',
  'model' => 'model',
  'installed' => 'installed',
  'id' => 'id',
);
$handler->display->display_options['style_options']['default'] = 'serial_number';
$handler->display->display_options['style_options']['info'] = array(
  'serial_number' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'label' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'capacity' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'model' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'installed' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 'striped',
  'bordered' => 0,
  'hover' => 0,
  'condensed' => 0,
);
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'No results markup';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = '<h3>Sorry, there are no tanks matching your search criteria</h3>';
$handler->display->display_options['empty']['area']['format'] = 'full_html';
/* Relationship: AQSTank: Relation: Owned by office (aqs_tank → redhen_org) */
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['id'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['table'] = 'aqs_tanks';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['field'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['r_index'] = '-1';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_left'] = 0;
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_right'] = 0;
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_county_target_id']['id'] = 'field_county_target_id';
$handler->display->display_options['relationships']['field_county_target_id']['table'] = 'field_data_field_county';
$handler->display->display_options['relationships']['field_county_target_id']['field'] = 'field_county_target_id';
$handler->display->display_options['relationships']['field_county_target_id']['relationship'] = 'relation_owns_tank_redhen_org';
/* Field: AQSTank: Serial Number */
$handler->display->display_options['fields']['serial_number']['id'] = 'serial_number';
$handler->display->display_options['fields']['serial_number']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['serial_number']['field'] = 'serial_number';
/* Field: Organization: Organization Name */
$handler->display->display_options['fields']['label']['id'] = 'label';
$handler->display->display_options['fields']['label']['table'] = 'redhen_org';
$handler->display->display_options['fields']['label']['field'] = 'label';
$handler->display->display_options['fields']['label']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['fields']['label']['label'] = 'Office';
/* Field: AQSTank: Tank Capacity */
$handler->display->display_options['fields']['capacity']['id'] = 'capacity';
$handler->display->display_options['fields']['capacity']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['capacity']['field'] = 'capacity';
$handler->display->display_options['fields']['capacity']['label'] = 'Capacity';
$handler->display->display_options['fields']['capacity']['precision'] = '0';
/* Field: AQSTank: Tank Model */
$handler->display->display_options['fields']['model']['id'] = 'model';
$handler->display->display_options['fields']['model']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['model']['field'] = 'model';
$handler->display->display_options['fields']['model']['label'] = 'Model';
/* Field: AQSTank: Date Installed */
$handler->display->display_options['fields']['installed']['id'] = 'installed';
$handler->display->display_options['fields']['installed']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['installed']['field'] = 'installed';
$handler->display->display_options['fields']['installed']['date_format'] = 'custom';
$handler->display->display_options['fields']['installed']['custom_date_format'] = 'm/d/Y';
$handler->display->display_options['fields']['installed']['second_date_format'] = 'long';
/* Field: AQSTank: Tank ID */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['label'] = '';
$handler->display->display_options['fields']['id']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['id']['alter']['text'] = '<a class="btn btn-primary btn-sm" href="/compliance/tanks/[serial_number]/log-service-record"><span class="glyphicon glyphicon-pencil"></span> Log Service</a>
<a href="/compliance/tanks/edit/[serial_number]" class="btn btn-link btn-sm"><span class="glyphicon glyphicon-edit"></span> edit</a>';
$handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
/* Filter criterion: Organization: Organization Name */
$handler->display->display_options['filters']['label']['id'] = 'label';
$handler->display->display_options['filters']['label']['table'] = 'redhen_org';
$handler->display->display_options['filters']['label']['field'] = 'label';
$handler->display->display_options['filters']['label']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['filters']['label']['operator'] = 'contains';
$handler->display->display_options['filters']['label']['group'] = 1;
$handler->display->display_options['filters']['label']['exposed'] = TRUE;
$handler->display->display_options['filters']['label']['expose']['operator_id'] = 'label_op';
$handler->display->display_options['filters']['label']['expose']['label'] = 'Office Name';
$handler->display->display_options['filters']['label']['expose']['operator'] = 'label_op';
$handler->display->display_options['filters']['label']['expose']['identifier'] = 'label';
$handler->display->display_options['filters']['label']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
);
$handler->display->display_options['filters']['label']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['label']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['label']['expose']['autocomplete_field'] = 'label';
$handler->display->display_options['filters']['label']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['label']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['label']['expose']['autocomplete_dependent'] = 0;
/* Filter criterion: Organization: Address - Locality (i.e. City) */
$handler->display->display_options['filters']['field_address_locality']['id'] = 'field_address_locality';
$handler->display->display_options['filters']['field_address_locality']['table'] = 'field_data_field_address';
$handler->display->display_options['filters']['field_address_locality']['field'] = 'field_address_locality';
$handler->display->display_options['filters']['field_address_locality']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['filters']['field_address_locality']['operator'] = 'contains';
$handler->display->display_options['filters']['field_address_locality']['group'] = 1;
$handler->display->display_options['filters']['field_address_locality']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_address_locality']['expose']['operator_id'] = 'field_address_locality_op';
$handler->display->display_options['filters']['field_address_locality']['expose']['label'] = 'City';
$handler->display->display_options['filters']['field_address_locality']['expose']['operator'] = 'field_address_locality_op';
$handler->display->display_options['filters']['field_address_locality']['expose']['identifier'] = 'field_address_locality';
$handler->display->display_options['filters']['field_address_locality']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  4 => 0,
  5 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['field_address_locality']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['field_address_locality']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['field_address_locality']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['field_address_locality']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['field_address_locality']['expose']['autocomplete_dependent'] = 0;
/* Filter criterion: AQS County: County Name */
$handler->display->display_options['filters']['name']['id'] = 'name';
$handler->display->display_options['filters']['name']['table'] = 'aqs_counties';
$handler->display->display_options['filters']['name']['field'] = 'name';
$handler->display->display_options['filters']['name']['relationship'] = 'field_county_target_id';
$handler->display->display_options['filters']['name']['operator'] = 'contains';
$handler->display->display_options['filters']['name']['group'] = 1;
$handler->display->display_options['filters']['name']['exposed'] = TRUE;
$handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['label'] = 'County';
$handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
$handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  4 => 0,
  5 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['name']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['name']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['name']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['name']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['name']['expose']['autocomplete_dependent'] = 0;
/* Filter criterion: Organization: Address - Administrative area (i.e. State / Province) */
$handler->display->display_options['filters']['field_address_administrative_area']['id'] = 'field_address_administrative_area';
$handler->display->display_options['filters']['field_address_administrative_area']['table'] = 'field_data_field_address';
$handler->display->display_options['filters']['field_address_administrative_area']['field'] = 'field_address_administrative_area';
$handler->display->display_options['filters']['field_address_administrative_area']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['filters']['field_address_administrative_area']['group'] = 1;
$handler->display->display_options['filters']['field_address_administrative_area']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['operator_id'] = 'field_address_administrative_area_op';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['label'] = 'State';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['operator'] = 'field_address_administrative_area_op';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['identifier'] = 'field_address_administrative_area';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
);
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['field_address_administrative_area']['expose']['autocomplete_dependent'] = 0;
