<?php
/**
 * @file
 * Forms for compliance tasks.
 */

/**
 * Form callback: create / edit a tank.
 *
 */
function aqs_compliance_tank_form($form, &$form_state, $tank = null, $office = NULL) {
  // Add the default field elements.
  $form_state['tank'] = $tank;
  $org = redhen_org_load($office);
  if (!empty($org)) {
    $form['office'] = array(
      '#type' => 'textfield',
      '#title' => t('Office'),
      '#default_value' => $org->label . ' (' . $org->org_id . ')',
      '#maxlength' => 255,
      '#required' => TRUE,
      '#attributes' => array('disabled'=>TRUE),
    );
  }
  else {
    if ($tank) {
      $office = relation_get_related_entity('aqs_tank', $tank->id, $relation_type = 'owns_tank');
    }

    $form['office'] = array(
      '#type' => 'textfield',
      '#title' => t('Office'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $office ?  $office->label . ' (' . $office->org_id . ')' : '',
      '#disabled' => !empty($office),
      '#autocomplete_path' => 'compliance/office/autocomplete',
    );
  }


  $form['serial_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Serial Number'),
    '#default_value' => $tank ?  $tank->serial_number : '',
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['model'] = array(
    '#type' => 'textfield',
    '#title' => t('Model'),
    '#default_value' => $tank ?  $tank->model : '',
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['capacity'] = array(
    '#type' => 'textfield',
    '#title' => t('Capacity (US gal)'),
    '#default_value' => $tank ?  $tank->capacity : null,
    '#maxlength' => 4,
    '#required' => TRUE,
  );

  $form['installed'] = array(
    '#type' => 'date',
    '#required' => TRUE,
    '#default_value' => $tank ?  array('year' => date('Y', $tank->installed), 'month' => date('n', $tank->installed), 'day' => date('j', $tank->installed)) : null,
    '#title' => t('Date of Installation'),
  );

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 40,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Tank'),
  );

  return $form;
}

/**
 * Validation callback
 */
function aqs_compliance_tank_form_validate($form, &$form_state) {
  // check for
  $office = $form_state['values']['office'];
  preg_match('/(.+) \((\d+)\)$/', $office , $matches);
  if (isset($matches[2]) && ($matches[2] > 0)) {
    $query = new EntityFieldQuery;
    $query->entityCondition('entity_type', 'redhen_org');
    $query->propertyCondition('org_id', $matches[2], '=');
    $results = $query->execute();
    if (empty($results)) {
      form_set_error('office', 'Office "' . $office . '" not found');
    }
  }
  else {
    form_set_error('office', 'Office "' . $office . '" not found');
  }

  if (empty($form_state['tank'])){
    $serial = $form_state['values']['serial_number'];
    $query = new EntityFieldQuery;
    $query->entityCondition('entity_type', 'aqs_tank')->propertyCondition('serial_number', $serial);
    $results = $query->execute();

    if(!empty($results)) {
      form_set_error('serial_number', 'Tank with serial number ' . $serial . ' already exists in system');
    }
  }

}

/**
 * Submit callback.
 */
function aqs_compliance_tank_form_submit($form, &$form_state) {
  if (empty($form_state['tank'])){
    $tank = entity_create('aqs_tank', array());
  }
  else {
    $tank = $form_state['tank'];
  }
  $tank->serial_number = $form_state['values']['serial_number'];
  $tank->model = $form_state['values']['model'];
  $tank->capacity = $form_state['values']['capacity'];
  $date = $form_state['values']['installed'];
  $tank->installed = mktime(0,0,0,$date['month'],$date['day'],$date['year']);

  $tank->save();

  if (empty($form_state['tank'])){
    //linking logic: parse out office id
    preg_match('/(.+) \((\d+)\)$/', $form_state['values']['office'], $matches);

    $endpoints = array();
    $endpoints[] = array('entity_type' => 'redhen_org', 'entity_id' => $matches[2]);
    $endpoints[] = array('entity_type' => 'aqs_tank', 'entity_id' => $tank->id);

    $new_relation = relation_create('owns_tank', $endpoints);

    if ($rid = relation_save($new_relation)) {
      //do nothing on success
      //drupal_set_message(t('Relation created'), 'status', FALSE);
    }
    else {
      drupal_set_message(t('Creating relation failed'), 'warning', FALSE);
    }
  }


  drupal_set_message(t('Tank %label saved.', array('%label' => $tank->serial_number)));
  $form_state['redirect'] = 'compliance/tanks';
}


/**
 * Form to log an event
 */
function aqs_compliance_log_message_form($form, &$form_state, $type, $tank = NULL, $tech = NULL, $office = NULL) {
  $message = message_create($type);
  $form_state['#entity'] = $message;
  $form_state['office'] = $office;
  $message_text = $message->view();

  field_attach_form('message', $message, $form, $form_state);

  if (!empty($tank)) {

    $query = new EntityFieldQuery;

    $query->entityCondition('entity_type', 'aqs_tank')->propertyCondition('serial_number', $tank);
    $results = $query->execute();
    $entities = entity_load('aqs_tank', array_keys(reset($results)));
    $entity = reset($entities);
    if ($entity) {
      $form['field_tank'][LANGUAGE_NONE]['#default_value'] = "$entity->serial_number ($entity->id)";
      $form['field_tank'][LANGUAGE_NONE]['#attributes']['disabled'] = TRUE;
    }
  }

  if (!empty($tech)) {
    $form['field_tech'][LANGUAGE_NONE]['#default_value'] = $tech;
    $form['field_tech'][LANGUAGE_NONE]['#attributes']['disabled'] = TRUE;
  }

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Log Record'),
      '#submit' => array('aqs_compliance_log_message_form_submit'),
    ),
  );

  return $form;
}

/**
 * Validate the submitted message.
 */
function aqs_compliance_log_message_form_validate($form, &$form_state) {
  field_attach_form_validate('message', $form_state['#entity'], $form, $form_state);
}

/**
 * Message submit function
 */
function aqs_compliance_log_message_form_submit($form, &$form_state) {
  $message = $form_state['#entity'];
  field_attach_submit('message', $message, $form, $form_state);
  $wrapper = entity_metadata_wrapper('message', $message);
  $wrapper->save();
  drupal_set_message(t('Record saved.'));
  $form_state['redirect'] = 'compliance';
  if (!empty($form_state['office'])){
    $form_state['redirect'] = 'compliance/offices/'.$form_state['office']->org_id;
  }
}

/**
 * Cleaner event form
 */
function aqs_compliance_log_cleaner_event_form($form, &$form_state) {
  $message = message_create('aqs_cleaner_use');
  $form_state['#entity'] = $message;
  field_attach_form('message', $message, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Log Record'),
      '#submit' => array('aqs_compliance_log_cleaner_event_form_submit'),
    ),
  );

  return $form;
}

/**
 * Cleaner event form validate
 */
function aqs_compliance_log_cleaner_event_form_validate($form, &$form_state) {
  field_attach_form_validate('message', $form_state['#entity'], $form, $form_state);
}

/**
 * Cleaner event form submit
 */
function aqs_compliance_log_cleaner_event_form_submit($form, &$form_state) {

  if ($form_state['cleaner']){
    $message = message_create('aqs_cleaner_use');
    $form['field_cleaner']['#parents'] = array('field_cleaner');
    form_set_value($form['field_cleaner'], array(LANGUAGE_NONE => array(0 => array('target_id' => $form_state['cleaner']->id))), $form_state);
    field_attach_submit('message', $message, $form, $form_state);
    $wrapper = entity_metadata_wrapper('message', $message);
    $wrapper->save();
  }
  else {
    $cleaners = $form_state['values']['field_cleaner'];
    $cleaners = reset($cleaners);
    foreach ($cleaners as $cleaner) {
      $message = message_create('aqs_cleaner_use');
      $form['field_cleaner']['#parents'] = array('field_cleaner');
      form_set_value($form['field_cleaner'], array(LANGUAGE_NONE => array(0 => $cleaner)), $form_state);
      field_attach_submit('message', $message, $form, $form_state);
      $wrapper = entity_metadata_wrapper('message', $message);
      $wrapper->save();
    }
  }


  drupal_set_message(t('Record saved.'));
  $form_state['redirect'] = 'compliance';
}


/**
 * Inspection event form
 */
function aqs_compliance_log_inspection_event_form($form, &$form_state, $tanks) {

  $form_state['tanks'] = $tanks;


  $form['tanks'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('tanks-section')),
    '#weight' => 40,
  );

  $values = array(
      'empty' => t('Empty'),
      'onethird' => t('1/3'),
      'twothirds' => t('2/3'),
      'full' => t('Full'),
    );

  foreach ($tanks as $tank) {
    $form['tanks']['tank'.$tank->id] = array(
      '#type' => 'fieldset',
      '#title' => t('Tank ' . $tank->serial_number),

    );

    $form['tanks']['tank'.$tank->id]['upper_tank_level'.$tank->id] = array(
      '#title' => t('Upper Tank Level'),
      '#type' => 'select',
      '#options' => $values,
      '#required' => TRUE,
    );

    $form['tanks']['tank'.$tank->id]['lower_tank_level'.$tank->id] = array(
      '#title' => t('Lower Tank Level'),
      '#type' => 'select',
      '#options' => $values,
      '#required' => TRUE,
    );

    $form['tanks']['tank'.$tank->id]['addcomment'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add Tank Comment'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['tanks']['tank'.$tank->id]['addcomment']['tank_comment'.$tank->id] = array(
      '#type' => 'textarea',
      '#title' => t('Comment'),
    );
  }

  $message = message_create('aqs_tank_inspection');
  $form_state['inspection_message'] = $message;
  field_attach_form('message', $message, $form, $form_state, NULL, array('field_name' => 'field_date'));

  $form['field_date']['#weight'] = 40;


  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Log Record'),
      '#submit' => array('aqs_compliance_log_inspection_event_form_submit'),
    ),
  );


  return $form;

}

/**
 * Inspection event form submit
 */
function aqs_compliance_log_inspection_event_form_submit($form, &$form_state) {
  $date = array(LANGUAGE_NONE => array(0 => array(
      'value' => $form_state['values']['field_date'][LANGUAGE_NONE][0]['value'],
      'timezone' => $form_state['values']['field_date'][LANGUAGE_NONE][0]['timezone'],
      'timezone_db' => 'UTC',
      'data_type' => 'datetime',
    )));
  $tanks = $form_state['tanks'];
  foreach($tanks as $tank) {
    $lower_level = $form_state['values']['lower_tank_level'.$tank->id];
    $upper_level = $form_state['values']['upper_tank_level'.$tank->id];
    $message = message_create('aqs_tank_inspection');
    $message->field_tank = array(LANGUAGE_NONE => array(0 => array('target_id' => $tank->id)));
    $message->field_tank_level = array(LANGUAGE_NONE => array(0 => array('value' => $lower_level)));
    $message->field_upper_tank_level = array(LANGUAGE_NONE => array(0 => array('value' => $upper_level)));
    $message->field_date = $date;
    $comment = $form_state['values']['tank_comment'.$tank->id];
    if (!empty($comment)) {
      $message->field_comment = array(LANGUAGE_NONE => array(0 =>array('value' => $comment)));
      $message->field_comment[LANGUAGE_NONE][0]['format'] = NULL;
      $message->field_comment[LANGUAGE_NONE][0]['safe_value'] = check_plain($comment);
    }
    $wrapper = entity_metadata_wrapper('message', $message);
    $wrapper->save();
  }

  drupal_set_message(t('Record saved.'));
  $form_state['redirect'] = 'compliance';
}

/**
 * Weekly combined report form
 */
function aqs_compliance_log_weekly_record_form($form, &$form_state, $office, $tanks) {

  $form_state['office'] = $office;
  $form_state['tanks'] = $tanks;

  $form['tanks'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('tanks-section')),
    '#weight' => 40,
  );

  $values = array(
      'empty' => t('Empty'),
      'onethird' => t('1/3'),
      'twothirds' => t('2/3'),
      'full' => t('Full'),
    );

  foreach ($tanks as $tank) {
    $form['tanks']['tank'.$tank->id] = array(
      '#type' => 'fieldset',
      '#title' => t('Tank ' . $tank->serial_number),

    );

    $form['tanks']['tank'.$tank->id]['upper_tank_level'.$tank->id] = array(
      '#title' => t('Upper Tank Level'),
      '#type' => 'select',
      '#options' => $values,
      '#required' => TRUE,
    );

    $form['tanks']['tank'.$tank->id]['lower_tank_level'.$tank->id] = array(
      '#title' => t('Lower Tank Level'),
      '#type' => 'select',
      '#options' => $values,
      '#required' => TRUE,
    );

    $form['tanks']['tank'.$tank->id]['addcomment'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add Tank Comment'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['tanks']['tank'.$tank->id]['addcomment']['tank_comment'.$tank->id] = array(
      '#type' => 'textarea',
      '#title' => t('Comment'),
    );
  }

  $message = message_create('aqs_cleaner_use');
  $form_state['cleaner_message'] = $message;
  field_attach_form('message', $message, $form, $form_state, NULL, array('field_name' => 'field_date'));
  field_attach_form('message', $message, $form, $form_state, NULL, array('field_name' => 'field_comment'));

  $form['cleaner'] = array(
      '#type' => 'fieldset',
      '#title' => t('Cleaner'),
    );

  $cleaner = (!empty($office->field_cleaner)) ? entity_load_single('aqs_cleaner', $office->field_cleaner[LANGUAGE_NONE][0]['target_id']) : NULL;
  $form_state['cleaner'] = $cleaner;

  if ($cleaner) {
    $form['cleaner']['cleaner_image'] = field_view_field('aqs_cleaner', $cleaner, 'field_image', array('label' => 'hidden', 'settings' => array('image_style' => 'thumbnail_medium')));
  }
  else {
    field_attach_form('message', $message, $form, $form_state, NULL, array('field_name' => 'field_cleaner'));
    $form['cleaner']['field_cleaner'] = $form['field_cleaner'];
    unset($form['field_cleaner']);
  }

  $form['cleaner']['addcomment'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add Cleaner Comment'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

  $form['cleaner']['addcomment']['field_comment'] = $form['field_comment'];
  unset($form['field_comment']);

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Log Record'),
      '#submit' => array('aqs_compliance_log_weekly_record_form_submit'),
    ),
  );


  return $form;

}

/**
 * Weekly combined report form submit
 */
function aqs_compliance_log_weekly_record_form_submit($form, &$form_state) {
  $date = array(LANGUAGE_NONE => array(0 => array(
      'value' => $form_state['values']['field_date'][LANGUAGE_NONE][0]['value'],
      'timezone' => $form_state['values']['field_date'][LANGUAGE_NONE][0]['timezone'],
      'timezone_db' => 'UTC',
      'data_type' => 'datetime',
    )));
  $tanks = $form_state['tanks'];
  foreach($tanks as $tank) {
    $lower_level = $form_state['values']['lower_tank_level'.$tank->id];
    $upper_level = $form_state['values']['upper_tank_level'.$tank->id];
    $message = message_create('aqs_tank_inspection');
    $message->field_tank = array(LANGUAGE_NONE => array(0 => array('target_id' => $tank->id)));
    $message->field_tank_level = array(LANGUAGE_NONE => array(0 => array('value' => $lower_level)));
    $message->field_upper_tank_level = array(LANGUAGE_NONE => array(0 => array('value' => $upper_level)));
    $message->field_date = $date;
    $comment = $form_state['values']['tank_comment'.$tank->id];
    if (!empty($comment)) {
      $message->field_comment = array(LANGUAGE_NONE => array(0 =>array('value' => $comment)));
      $message->field_comment[LANGUAGE_NONE][0]['format'] = NULL;
      $message->field_comment[LANGUAGE_NONE][0]['safe_value'] = check_plain($comment);
    }
    $wrapper = entity_metadata_wrapper('message', $message);
    $wrapper->save();
  }

  $office = $form_state['office'];
  $oid = $office->internalIdentifier();

  if ($form_state['cleaner']){
    $message = message_create('aqs_cleaner_use');
    $message->field_cleaner = array(LANGUAGE_NONE => array(0 => array('target_id' => $form_state['cleaner']->id)));
    $message->field_office = array(LANGUAGE_NONE => array(0 => array('target_id' => $oid)));
    $message->field_date = $date;
    $comment = $form_state['values']['field_comment'];
    if (!empty($comment[LANGUAGE_NONE][0]['value'])) {
      $message->field_comment = $comment;
      $message->field_comment[LANGUAGE_NONE][0]['format'] = NULL;
      $message->field_comment[LANGUAGE_NONE][0]['safe_value'] = check_plain($comment[LANGUAGE_NONE][0]['value']);
    }
    $wrapper = entity_metadata_wrapper('message', $message);
    $wrapper->save();
  }
  else {
    $cleaners = $form_state['values']['field_cleaner'];
    $cleaners = reset($cleaners);
    foreach ($cleaners as $cleaner) {
      $message = message_create('aqs_cleaner_use');
      $message->field_cleaner = array(LANGUAGE_NONE => array(0 => $cleaner));
      $message->field_office = array(LANGUAGE_NONE => array(0 => array('target_id' => $oid)));
      $message->field_date = $date;
      $comment = $form_state['values']['field_comment'];
      if (!empty($comment[LANGUAGE_NONE][0]['value'])) {
        $message->field_comment = $comment;
        $message->field_comment[LANGUAGE_NONE][0]['format'] = NULL;
        $message->field_comment[LANGUAGE_NONE][0]['safe_value'] = check_plain($comment[LANGUAGE_NONE][0]['value']);
      }
      $wrapper = entity_metadata_wrapper('message', $message);
      $wrapper->save();
    }
  }

  drupal_set_message(t('Record saved.'));
  $form_state['redirect'] = 'compliance';
}

/**
 * Edit message form
 */
function aqs_compliance_message_edit_form($form, &$form_state, $message){
  // Add the default field elements.

  // Add the field related form elements.
  $form_state['message'] = $message;
  field_attach_form('message', $message, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 40,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save event'),
  );


  return $form;
}

/**
 * Validation callback for aqs_compliance_message_edit_form().
 */
function aqs_compliance_message_edit_form_validate($form, &$form_state) {
    $message = $form_state['message'];

    // Notify field widgets to validate their data.
    field_attach_form_validate('message', $message, $form, $form_state);
}

/**
 * Submit callback for aqs_compliance_message_edit_form().
 */
function aqs_compliance_message_edit_form_submit($form, &$form_state) {
    $message = &$form_state['message'];

    // Notify field widgets.
    field_attach_submit('message', $message, $form, $form_state);

    // Save the message
    $wrapper = entity_metadata_wrapper('message', $message);
    $wrapper->save();
    //$message->save();

    drupal_set_message(t('Event updated.'));
    $form_state['redirect'] = 'compliance';
}

/**
 * Form callback: create or edit a client.
 *
 * @param $org
 *   The organization object to edit or for a create form an empty organization object
 *     with only an org type defined.
 */
function aqs_compliance_org_form($form, &$form_state, $org) {
  // Add the default field elements.
  $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $org->label,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#weight' => -6,
  );

  $form['district'] = array(
    '#title' => t('County'),
    '#type' => 'select',
    '#options' => aqs_compliance_dashboard_get_counties(),
    '#default_value' => isset($org->field_county) ? $org->field_county[LANGUAGE_NONE][0]['target_id'] : null,
    '#required' => TRUE,
    '#ajax' => array(
      'event' => 'change',
      'wrapper' => 'municipality_wrapper',
      'callback' => 'aqs_compliance_dashboard_municipalities_ajax',
      'method' => 'replace',
    ),
  );


  $form['municipality_wrapper'] = array('#type' => 'container', '#prefix' => '<div id="municipality_wrapper">', '#suffix' => '</div>');

  $municipalities = array();

  if (isset($form_state['values']['district']) || isset($org->field_county)) {
    if (isset($form_state['values']['district'])) {
      $municipalities = aqs_compliance_dashboard_get_municipalities($form_state['values']['district']);
    }
    else {
      $municipalities = aqs_compliance_dashboard_get_municipalities($org->field_county[LANGUAGE_NONE][0]['target_id']);
    }
  }

  $form['municipality_wrapper']['municipality'] = array(
      '#title' => t('Water Treatment Plant'),
      '#type' => 'select',
      '#options' => $municipalities,
      '#default_value' => isset($org->field_plant) ? $org->field_plant[LANGUAGE_NONE][0]['target_id'] : null,
      '#required' => FALSE,
      '#disabled' => empty($municipalities),
    );

  // Add the field related form elements.
  $form_state['redhen_org'] = $org;
  field_attach_form('redhen_org', $org, $form, $form_state);

  $form['actions'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('form-actions')),
      '#weight' => 40,
  );

  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save organization'),
    '#validate' => array('aqs_compliance_org_form_validate_noajax'),
  );


  return $form;
}

/**
 * Validation callback for aqs_compliance_org_form().
 */
function aqs_compliance_org_form_validate_noajax($form, &$form_state) {
  $form_state['values']['field_county'][LANGUAGE_NONE][0]['target_id'] = $form_state['values']['district'];
  if ($form_state['values']['municipality'] != 0){
    $form_state['values']['field_plant'][LANGUAGE_NONE][0]['target_id'] = $form_state['values']['municipality'];
  }

  $org = $form_state['redhen_org'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('redhen_org', $org, $form, $form_state);
}

/**
 * Submit callback for aqs_compliance_org_form().
 */
function aqs_compliance_org_form_submit($form, &$form_state) {

    $org = &$form_state['redhen_org'];

    // Set the org's author uid.
    global $user;
    $org->author_uid = $user->uid;

    // Save default parameters back into the $org object.
    $org->label = $form_state['values']['label'];

    // Notify field widgets.
    field_attach_submit('redhen_org', $org, $form, $form_state);

    $org->field_county = array(LANGUAGE_NONE => array(0 => array('target_id' => $form_state['values']['district'])));
    if ($form_state['values']['municipality'] != 0){
      $org->field_plant = array(LANGUAGE_NONE => array(0 => array('target_id' => $form_state['values']['municipality'])));
    }

    // Save the organization.
    $org->save();

    drupal_set_message(t('%label saved.', array('%label' => $org->label)));
    $form_state['redirect'] = 'compliance/offices/' . $org->org_id;
}


function aqs_compliance_org_image_form($form, &$form_state, $org) {
  // Add the field related form elements.
  $form_state['redhen_org'] = $org;
  field_attach_form('redhen_org', $org, $form, $form_state, NULL, array('field_name' => 'field_image'));

  $form['actions'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('form-actions')),
      '#weight' => 40,
  );

  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Images'),
  );


  return $form;
}

/**
 * Validation callback for aqs_compliance_org_image_form().
 */
function aqs_compliance_org_image_form_validate($form, &$form_state) {
  $org = $form_state['redhen_org'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('redhen_org', $org, $form, $form_state);
}

/**
 * Submit callback for aqs_compliance_org_image_form().
 */

function aqs_compliance_org_image_form_submit($form, &$form_state) {
  $org = $form_state['redhen_org'];
  // Notify field widgets.
  field_attach_submit('redhen_org', $org, $form, $form_state);

  // Save the organization.
  $org->save();

  drupal_set_message(t('Images saved.'));
  $form_state['redirect'] = 'compliance/offices/' . $org->org_id;
}

/**
 * Form callback: create or edit a contact.
 *
 * @param $contact
 *   The contact object to edit or for a create form an empty contact object
 *     with only a contact type defined.
 */
function aqs_compliance_contact_form($form, &$form_state, $contact, $office = NULL) {

  // Add the default field elements.
  $form['name'] = array('#type' => 'container');
  $form['name']['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First name'),
    '#default_value' => $contact->first_name,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -6,
  );
  $form['name']['middle_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Middle name'),
    '#default_value' => $contact->middle_name,
    '#maxlength' => 255,
    '#weight' => -6,
  );
  $form['name']['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last name'),
    '#default_value' => $contact->last_name,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => 5,
  );

  // Add the field related form elements.
  $form_state['redhen_contact'] = $contact;
  $form_state['office'] = $office;

  field_attach_form('redhen_contact', $contact, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 40,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array('aqs_compliance_contact_form_submit');

  if (!empty($form['#submit'])) {
    $submit = array_merge($submit, $form['#submit']);
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save contact'),
    '#submit' => $submit,
  );

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'aqs_compliance_contact_form_validate';

  return $form;
}

/**
 * Validation callback for aqs_compliance_contact_form().
 */
function aqs_compliance_contact_form_validate($form, &$form_state) {
  $contact = $form_state['redhen_contact'];
  // attach office here

  // Notify field widgets to validate their data.
  field_attach_form_validate('redhen_contact', $contact, $form, $form_state);

  // If mirroring a connected and contact email's, ensure the email is not used
  // by anther Drupal user.
  if (variable_get('redhen_contact_mirror_email', FALSE) && $contact->uid) {
    foreach ($form_state['values'][REDHEN_CONTACT_EMAIL_FIELD][LANGUAGE_NONE] as $email) {
      if (is_array($email) && !empty($email['value']) && $email['default']) {
        $user = user_load_by_mail($email['value']);
        if ($user && ($user->uid != $contact->uid)) {
          form_set_error(
            REDHEN_CONTACT_EMAIL_FIELD,
            t('This contact\'s primary email is configured to mirror the associated Drupal user and email address %email is already in use by another Drupal user.',
              array('%email' => $email['value'])
            )
          );
          break;
        }
      }
    }
  }
}

/**
 * Submit callback for aqs_compliance_contact_form().
 */
function aqs_compliance_contact_form_submit($form, &$form_state) {
  $contact = &$form_state['redhen_contact'];
  $office = &$form_state['office'];

  // Set the contact's author uid
  global $user;
  $contact->author_uid = $user->uid;

  // Save default parameters back into the $contact object.
  $contact->first_name = $form_state['values']['first_name'];
  $contact->middle_name = $form_state['values']['middle_name'];
  $contact->last_name = $form_state['values']['last_name'];

  // Notify field widgets.
  field_attach_submit('redhen_contact', $contact, $form, $form_state);

  // Save the contact.
  $contact = redhen_contact_save($contact);

  $wrapper = entity_metadata_wrapper('redhen_contact', $contact);

  $related = redhen_relation_relations($contact, $relation_type = 'redhen_affiliation');

  if(empty($related)){
    $endpoints = array();
    $endpoints[] = array('entity_type' => 'redhen_contact', 'entity_id' => $wrapper->getIdentifier());
    $endpoints[] = array('entity_type' => 'redhen_org', 'entity_id' => $office->org_id);

    $new_relation = relation_create('redhen_affiliation', $endpoints);

    if ($rid = relation_save($new_relation)) {
      //do nothing on success
      //drupal_set_message(t('Relation created'), 'status', FALSE);
    }
    else {
      drupal_set_message(t('Creating relation failed'), 'warning', FALSE);
    }
  }


  drupal_set_message(t('Dental user %name saved.', array('%name' => $wrapper->full_name->value())));
  $form_state['redirect'] = 'compliance/offices/'. $office->org_id;
}

/**
 * User password form
 */
function aqs_compliance_user_password_form($form, &$form_state, $user) {
  $protected_values['pass'] = t('Password');
  $form['#user'] = $user;
  $form['account']['current_pass_required_values'] = array(
        '#type' => 'value',
        '#value' => $protected_values,
      );
      $form['account']['current_pass'] = array(
        '#type' => 'password',
        '#title' => t('Current password'),
        '#size' => 25,
        '#access' => !empty($protected_values),
        '#description' => 'Enter your current password',
        '#weight' => -5,
        // Do not let web browsers remember this password, since we are trying
        // to confirm that the person submitting the form actually knows the
        // current one.
        '#attributes' => array('autocomplete' => 'off'),
      );
  $form['account']['pass'] = array(
      '#type' => 'password_confirm',
      '#size' => 25,
      '#description' => t('To change the current user password, enter the new password in both fields.'),
    );
  $form['#validate'][] = 'user_validate_current_pass';

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update details'),
    '#submit' => array('aqs_compliance_user_password_form_submit'),
  );

  return $form;
}

/**
 * User password form submit
 */
function aqs_compliance_user_password_form_submit($form, &$form_state) {
  $edit['pass'] = $form_state['values']['pass'];
  user_save($form['#user'], $edit);
  $form_state['redirect'] = '/user';
}


/**
 * Inline form for counties
 */
function aqs_compliance_county_form($form, &$form_state, $county = NULL) {
  $form['view'] = array(
    '#markup' => views_embed_view('aqs_counties'),
  );
  $form['name'] = array(
    '#title' => t('County name'),
    '#type' => 'textfield',
    '#default_value' => isset($county->name) ? $county->name : '',
    '#required' => TRUE,
  );
  if (!isset($county)) {
    $county = entity_create('aqs_county', array());
  }
  field_attach_form('aqs_county', $county, $form, $form_state);

  $form_state['aqs_county'] = $county;

  $form['add_county'] = array(
      '#type' => 'submit',
      '#value' => t('Add County'),
      '#weight' => 30,
      '#ajax' => array(
        'callback' => 'aqs_compliance_county_form_ajax_submit',
        'wrapper' => 'aqs-compliance-county-form',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );

  return $form;
}

/**
 * Ajax callback function.
 */
function aqs_compliance_county_form_ajax_submit($form, $form_state) {
  $error = form_get_errors();
  if (empty($error)){
    $county = $form_state['aqs_county'];
    $county->name = $form_state['values']['name'];
    field_attach_submit('aqs_county', $county, $form, $form_state);
    $county->save();
    unset($form['view']);
    $form['view'] = array(
      '#markup' => views_embed_view('aqs_counties'),
    );

  }

  return $form;
}

/**
 * Inline form for plants
 */
function aqs_compliance_plant_form($form, &$form_state, $plant = NULL) {
  $form['view'] = array(
    '#markup' => views_embed_view('aqs_plants'),
  );
  $form['name'] = array(
    '#title' => t('Plant name'),
    '#type' => 'textfield',
    '#default_value' => isset($plant->name) ? $plant->name : '',
    '#required' => TRUE,
  );
  if (!isset($plant)) {
    $plant = entity_create('aqs_plant', array());
  }
  field_attach_form('aqs_plant', $plant, $form, $form_state);

  $form_state['aqs_plant'] = $plant;

  $form['add_plant'] = array(
      '#type' => 'submit',
      '#value' => t('Add Plant'),
      '#weight' => 30,
      '#ajax' => array(
        'callback' => 'aqs_compliance_plant_form_ajax_submit',
        'wrapper' => 'aqs-compliance-plant-form',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );

  return $form;
}

/**
 * Ajax callback function.
 */
function aqs_compliance_plant_form_ajax_submit($form, $form_state) {
  $error = form_get_errors();
  if (empty($error)){
    $plant = $form_state['aqs_plant'];
    $plant->name = $form_state['values']['name'];
    field_attach_submit('aqs_plant', $plant, $form, $form_state);
    $plant->save();
    unset($form['view']);
    $form['view'] = array(
      '#markup' => views_embed_view('aqs_plants'),
    );
  }

  return $form;
}
