<?php

/**
 * Implements hook_entity_info().
 */
function aqs_tank_entity_info() {
  
  $info = array();
  
  $info['aqs_tank'] = array(
    'label' => t('AQSTank'),
    'base table' => 'aqs_tanks',
    'entity keys' => array(
      'id' => 'id',
      'label' => 'serial_number',
    ),
    'entity class' => 'Entity',
    'controller class' => 'AQSTankEntityController',
    'views controller class' => 'EntityDefaultViewsController',
    'module' => 'aqs_tank',
  );
  
  return $info;
}

/**
 * Implements hook_entity_property_info().
 */
function aqs_tank_entity_property_info() {

  $info = array();

  $info['aqs_tank']['properties']['id'] = array(
    'label' => t('Tank ID'),
    'description' => t('The ID of the tank.'),
    'type' => 'integer',
    'schema field' => 'id',
  );
  $info['aqs_tank']['properties']['model'] = array(
    'label' => t('Tank Model'),
    'description' => t('Model of tank.'),
    'type' => 'text',
    'schema field' => 'model',
  );
  $info['aqs_tank']['properties']['serial_number'] = array(
    'label' => t('Serial Number'),
    'description' => t('The serial number assigned to tank.'),
    'type' => 'text',
    'schema field' => 'serial_number',
  );
  $info['aqs_tank']['properties']['capacity'] = array(
    'label' => t('Tank Capacity'),
    'description' => t('Capacity of tank in US Gallons.'),
    'type' => 'decimal',
    'schema field' => 'capacity',
  );
  $info['aqs_tank']['properties']['installed'] = array(
    'label' => t('Date Installed'),
    'description' => t('Date the tank was installed.'),
    'type' => 'date',
    'schema field' => 'installed',
  );

  return $info;
}


/**
 * Implements hook_menu()
 */
function aqs_tank_menu() {
  $items = array();
  
  $items['tanks'] = array(
    'title' => 'Tanks demo',
    'page callback' => 'aqs_tank_tanks',
    'access arguments' => array('access content'),
  );
  
  return $items;
}

/**
 * Callback function for our project entities test path
 */
function aqs_tank_tanks() {
  
  
  $tanks = entity_load('aqs_tank', array(1));
  
  // Saving new entities 
  if (!isset($tanks[1])) {
    $entity = entity_create('aqs_tank', array('id' => 1));
    $entity->model = t('aqs001');
    $entity->serial_number = t('0001');
    $entity->installed = '1397501132';
    $entity->capacity = '5.2';
    $entity->save();
  }
  
  // Listing entities
  $list = entity_view('aqs_tank', $tanks);
  
  $output = array();
  foreach ($list['aqs_tank'] as $tank) {
    $output[] = drupal_render($tank);
  }
  
  return implode($output);

}

/**
   * Extending the EntityAPIController for the AQSTank entity.
   */
  class AQSTankEntityController extends EntityAPIController {
    
    public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
  
      $build = parent::buildContent($entity, $view_mode, $langcode, $content);
      
      // Our additions to the $build render array.
      $build['serial'] = array(
        '#type' => 'markup',
        '#markup' => check_plain($entity->serial_number),
        '#prefix' => '<p>Serial Number:',
        '#suffix' => '</p>',
      );

      $build['model'] = array(
        '#type' => 'markup',
        '#markup' => check_plain($entity->model),
        '#prefix' => '<p>Model: ',
        '#suffix' => '</p>',
      );

      $build['installed_date'] = array(
        '#type' => 'markup',
        '#markup' => date('d F, Y', check_plain($entity->installed)),
        '#prefix' => '<p>Installed: ',
        '#suffix' => '</p>',
      );

      $build['capacity'] = array(
        '#type' => 'markup',
        '#markup' => check_plain($entity->capacity),
        '#prefix' => '<p>Capacity: ',
        '#suffix' => 'gal</p>',
      );
      
      return $build;
    
    }
    
  }
