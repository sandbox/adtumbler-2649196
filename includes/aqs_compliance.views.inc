<?php
/**
 * @file
 * Extends views for compliance tasks.
 */

/**
* @implements hook_views_pre_view().
*/
function aqs_compliance_views_pre_view(&$view, &$display_id, &$args){
  if($view->name == "single_tank") {

    global $user;
    $user_contact = redhen_contact_load_by_user($user);

    if ($user_contact && $user_contact->type == 'dental_user') {
      $args[] = $user_contact->contact_id;
    }
  }
}
