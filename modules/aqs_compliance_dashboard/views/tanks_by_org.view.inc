<?php

$view = new view();
$view->name = 'tanks_by_org';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'aqs_tanks';
$view->human_name = 'Tanks by org';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Tanks by org';
$handler->display->display_options['css_class'] = 'table-responsive';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'mini';
$handler->display->display_options['pager']['options']['items_per_page'] = '3';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: AQSTank: Relation: Owned by office (aqs_tank → redhen_org) */
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['id'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['table'] = 'aqs_tanks';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['field'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['r_index'] = '-1';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_left'] = 0;
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_right'] = 0;
/* Field: AQSTank: Tank ID */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['exclude'] = TRUE;
/* Field: AQSTank: Serial Number */
$handler->display->display_options['fields']['serial_number']['id'] = 'serial_number';
$handler->display->display_options['fields']['serial_number']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['serial_number']['field'] = 'serial_number';
$handler->display->display_options['fields']['serial_number']['label'] = '';
$handler->display->display_options['fields']['serial_number']['exclude'] = TRUE;
$handler->display->display_options['fields']['serial_number']['element_label_colon'] = FALSE;
/* Field: AQSTank: Tank Model */
$handler->display->display_options['fields']['model']['id'] = 'model';
$handler->display->display_options['fields']['model']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['model']['field'] = 'model';
$handler->display->display_options['fields']['model']['label'] = '';
$handler->display->display_options['fields']['model']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['model']['alter']['text'] = '<h4>Tank [serial_number]</h4>
<a href=\'/compliance/tanks/[serial_number]/log-service-record\'>Log Service Record</a>';
$handler->display->display_options['fields']['model']['element_label_colon'] = FALSE;
/* Contextual filter: Organization: Organization ID */
$handler->display->display_options['arguments']['org_id']['id'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['arguments']['org_id']['field'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['arguments']['org_id']['default_action'] = 'default';
$handler->display->display_options['arguments']['org_id']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['org_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['org_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['org_id']['summary_options']['items_per_page'] = '25';

/* Display: Embed */
$handler = $view->new_display('embed', 'Embed', 'embed_1');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['columns'] = array(
  'serial_number' => 'serial_number',
  'model' => 'model',
  'installed' => 'installed',
  'capacity' => 'capacity',
  'id' => 'id',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'serial_number' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'model' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'installed' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'capacity' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 'striped',
  'bordered' => 0,
  'hover' => 0,
  'condensed' => 0,
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: AQSTank: Serial Number */
$handler->display->display_options['fields']['serial_number']['id'] = 'serial_number';
$handler->display->display_options['fields']['serial_number']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['serial_number']['field'] = 'serial_number';
$handler->display->display_options['fields']['serial_number']['label'] = 'Tank';
$handler->display->display_options['fields']['serial_number']['element_label_colon'] = FALSE;
/* Field: AQSTank: Tank Model */
$handler->display->display_options['fields']['model']['id'] = 'model';
$handler->display->display_options['fields']['model']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['model']['field'] = 'model';
$handler->display->display_options['fields']['model']['label'] = 'Model';
$handler->display->display_options['fields']['model']['alter']['text'] = '<h4>Tank [serial_number]</h4>
<a href=\'/compliance/logrecord/tank/[serial_number]\'>Log Service Record</a>';
$handler->display->display_options['fields']['model']['element_label_colon'] = FALSE;
/* Field: AQSTank: Date Installed */
$handler->display->display_options['fields']['installed']['id'] = 'installed';
$handler->display->display_options['fields']['installed']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['installed']['field'] = 'installed';
$handler->display->display_options['fields']['installed']['date_format'] = 'custom';
$handler->display->display_options['fields']['installed']['custom_date_format'] = 'm/d/Y';
$handler->display->display_options['fields']['installed']['second_date_format'] = 'long';
/* Field: AQSTank: Tank Capacity */
$handler->display->display_options['fields']['capacity']['id'] = 'capacity';
$handler->display->display_options['fields']['capacity']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['capacity']['field'] = 'capacity';
$handler->display->display_options['fields']['capacity']['label'] = 'Capacity';
$handler->display->display_options['fields']['capacity']['precision'] = '0';
$handler->display->display_options['fields']['capacity']['suffix'] = ' gallons';
/* Field: AQSTank: Tank ID */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['label'] = '';
$handler->display->display_options['fields']['id']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['id']['alter']['text'] = '<a class="btn btn-primary btn-sm" href="/compliance/offices/!1/log-service-record/[serial_number]"><span class="glyphicon glyphicon-pencil"></span> Log Service</a>
<a href="/compliance/tanks/edit/[serial_number]" class="btn btn-link btn-sm"><span class="glyphicon glyphicon-edit"></span> edit</a>';
$handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
