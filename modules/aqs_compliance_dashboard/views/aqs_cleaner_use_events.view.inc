<?php

$view = new view();
$view->name = 'aqs_cleaner_use_events';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'message';
$view->human_name = 'AQS Cleaner Use Events';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['css_class'] = 'table-responsive';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['columns'] = array(
  'field_date' => 'field_date',
  'field_office' => 'field_office',
  'field_cleaner' => 'field_cleaner',
  'field_comment' => 'field_comment',
  'mid' => 'mid',
);
$handler->display->display_options['style_options']['default'] = 'field_date';
$handler->display->display_options['style_options']['info'] = array(
  'field_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_office' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_cleaner' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_comment' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'mid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 'striped',
  'condensed' => 'condensed',
  'bordered' => 0,
  'hover' => 0,
);
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'No events found';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Field: Message: Time and Date */
$handler->display->display_options['fields']['field_date']['id'] = 'field_date';
$handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
$handler->display->display_options['fields']['field_date']['field'] = 'field_date';
$handler->display->display_options['fields']['field_date']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Field: Cleaner */
$handler->display->display_options['fields']['field_cleaner']['id'] = 'field_cleaner';
$handler->display->display_options['fields']['field_cleaner']['table'] = 'field_data_field_cleaner';
$handler->display->display_options['fields']['field_cleaner']['field'] = 'field_cleaner';
$handler->display->display_options['fields']['field_cleaner']['settings'] = array(
  'link' => 0,
);
$handler->display->display_options['fields']['field_cleaner']['delta_offset'] = '0';
/* Field: Message: Comment */
$handler->display->display_options['fields']['field_comment']['id'] = 'field_comment';
$handler->display->display_options['fields']['field_comment']['table'] = 'field_data_field_comment';
$handler->display->display_options['fields']['field_comment']['field'] = 'field_comment';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
$handler->display->display_options['fields']['mid']['label'] = '';
$handler->display->display_options['fields']['mid']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['mid']['alter']['text'] = '<a href="/compliance/events/[mid]/edit" class="btn btn-link btn-sm"><span class="glyphicon glyphicon-edit"></span> edit</a>';
$handler->display->display_options['fields']['mid']['element_label_colon'] = FALSE;
/* Contextual filter: Message: Office (field_office) */
$handler->display->display_options['arguments']['field_office_target_id']['id'] = 'field_office_target_id';
$handler->display->display_options['arguments']['field_office_target_id']['table'] = 'field_data_field_office';
$handler->display->display_options['arguments']['field_office_target_id']['field'] = 'field_office_target_id';
$handler->display->display_options['arguments']['field_office_target_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['field_office_target_id']['exception']['value'] = '';
$handler->display->display_options['arguments']['field_office_target_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_office_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_office_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_office_target_id']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Message: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'message';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'aqs_cleaner_use' => 'aqs_cleaner_use',
);

/* Display: dental_user */
$handler = $view->new_display('embed', 'dental_user', 'dental_user');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Message: Time and Date */
$handler->display->display_options['fields']['field_date']['id'] = 'field_date';
$handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
$handler->display->display_options['fields']['field_date']['field'] = 'field_date';
$handler->display->display_options['fields']['field_date']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Field: Cleaner */
$handler->display->display_options['fields']['field_cleaner']['id'] = 'field_cleaner';
$handler->display->display_options['fields']['field_cleaner']['table'] = 'field_data_field_cleaner';
$handler->display->display_options['fields']['field_cleaner']['field'] = 'field_cleaner';
$handler->display->display_options['fields']['field_cleaner']['settings'] = array(
  'link' => 0,
);
$handler->display->display_options['fields']['field_cleaner']['delta_offset'] = '0';
/* Field: Message: Comment */
$handler->display->display_options['fields']['field_comment']['id'] = 'field_comment';
$handler->display->display_options['fields']['field_comment']['table'] = 'field_data_field_comment';
$handler->display->display_options['fields']['field_comment']['field'] = 'field_comment';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
$handler->display->display_options['fields']['mid']['label'] = '';
$handler->display->display_options['fields']['mid']['exclude'] = TRUE;
$handler->display->display_options['fields']['mid']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['mid']['alter']['text'] = '<a href="/compliance/events/[mid]/edit" class="btn btn-link btn-sm"><span class="glyphicon glyphicon-edit"></span> edit</a>';
$handler->display->display_options['fields']['mid']['element_label_colon'] = FALSE;

/* Display: aqs_technician */
$handler = $view->new_display('embed', 'aqs_technician', 'aqs_technician');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Message: Time and Date */
$handler->display->display_options['fields']['field_date']['id'] = 'field_date';
$handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
$handler->display->display_options['fields']['field_date']['field'] = 'field_date';
$handler->display->display_options['fields']['field_date']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Message: Office */
$handler->display->display_options['fields']['field_office']['id'] = 'field_office';
$handler->display->display_options['fields']['field_office']['table'] = 'field_data_field_office';
$handler->display->display_options['fields']['field_office']['field'] = 'field_office';
$handler->display->display_options['fields']['field_office']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['field_office']['alter']['path'] = 'compliance/offices/[field_office-target_id]';
$handler->display->display_options['fields']['field_office']['settings'] = array(
  'link' => 0,
);
/* Field: Field: Cleaner */
$handler->display->display_options['fields']['field_cleaner']['id'] = 'field_cleaner';
$handler->display->display_options['fields']['field_cleaner']['table'] = 'field_data_field_cleaner';
$handler->display->display_options['fields']['field_cleaner']['field'] = 'field_cleaner';
$handler->display->display_options['fields']['field_cleaner']['settings'] = array(
  'link' => 0,
);
$handler->display->display_options['fields']['field_cleaner']['delta_offset'] = '0';
/* Field: Message: Comment */
$handler->display->display_options['fields']['field_comment']['id'] = 'field_comment';
$handler->display->display_options['fields']['field_comment']['table'] = 'field_data_field_comment';
$handler->display->display_options['fields']['field_comment']['field'] = 'field_comment';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
$handler->display->display_options['fields']['mid']['label'] = '';
$handler->display->display_options['fields']['mid']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['mid']['alter']['text'] = '<a href="/compliance/events/[mid]/edit" class="btn btn-link btn-sm"><span class="glyphicon glyphicon-edit"></span> edit</a>';
$handler->display->display_options['fields']['mid']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;

/* Display: by_office */
$handler = $view->new_display('embed', 'by_office', 'by_office');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Message: Office (field_office) */
$handler->display->display_options['arguments']['field_office_target_id']['id'] = 'field_office_target_id';
$handler->display->display_options['arguments']['field_office_target_id']['table'] = 'field_data_field_office';
$handler->display->display_options['arguments']['field_office_target_id']['field'] = 'field_office_target_id';
$handler->display->display_options['arguments']['field_office_target_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['field_office_target_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_office_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_office_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_office_target_id']['summary_options']['items_per_page'] = '25';
