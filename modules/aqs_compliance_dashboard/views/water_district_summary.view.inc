<?php

$view = new view();
$view->name = 'water_district_summary';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'message';
$view->human_name = 'Water District Summary';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Water District';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  6 => '6',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Clear';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'views_aggregator';
$handler->display->display_options['style_options']['columns'] = array(
  'mid' => 'mid',
  'field_date' => 'field_date',
  'label' => 'label',
  'field_tank' => 'field_tank',
  'field_gallons_removed' => 'field_gallons_removed',
  'expression' => 'expression',
);
$handler->display->display_options['style_options']['default'] = 'field_date';
$handler->display->display_options['style_options']['info'] = array(
  'mid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'field_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'label' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 1,
    'aggr' => array(
      'views_aggregator_group_and_compress' => 'views_aggregator_group_and_compress',
    ),
    'aggr_par' => 'Office',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'field_tank' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'field_gallons_removed' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 1,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => 'Total',
  ),
  'expression' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 1,
    'aggr' => array(
      'views_aggregator_sum' => 'views_aggregator_sum',
    ),
    'aggr_par' => 'Sum',
    'has_aggr_column' => 1,
    'aggr_column' => 'views_aggregator_expression',
    'aggr_par_column' => '[field_gallons_removed]*0.8',
  ),
);
$handler->display->display_options['style_options']['column_aggregation']['totals_per_page'] = '0';
$handler->display->display_options['style_options']['column_aggregation']['totals_row_position'] = array(
  1 => 0,
  2 => '2',
);
$handler->display->display_options['style_options']['column_aggregation']['precision'] = '2';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_tank_target_id']['id'] = 'field_tank_target_id';
$handler->display->display_options['relationships']['field_tank_target_id']['table'] = 'field_data_field_tank';
$handler->display->display_options['relationships']['field_tank_target_id']['field'] = 'field_tank_target_id';
$handler->display->display_options['relationships']['field_tank_target_id']['label'] = 'Tank entity referenced from message';
/* Relationship: AQSTank: Relation: Owned by office (aqs_tank → redhen_org) */
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['id'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['table'] = 'aqs_tanks';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['field'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['relationship'] = 'field_tank_target_id';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['r_index'] = '-1';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_left'] = 0;
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_right'] = 0;
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_county_target_id']['id'] = 'field_county_target_id';
$handler->display->display_options['relationships']['field_county_target_id']['table'] = 'field_data_field_county';
$handler->display->display_options['relationships']['field_county_target_id']['field'] = 'field_county_target_id';
$handler->display->display_options['relationships']['field_county_target_id']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['field_county_target_id']['label'] = 'AQS County entity referenced from office';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
$handler->display->display_options['fields']['mid']['exclude'] = TRUE;
/* Field: Message: Time and Date */
$handler->display->display_options['fields']['field_date']['id'] = 'field_date';
$handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
$handler->display->display_options['fields']['field_date']['field'] = 'field_date';
$handler->display->display_options['fields']['field_date']['label'] = 'Date';
$handler->display->display_options['fields']['field_date']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_date']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Organization: Organization Name */
$handler->display->display_options['fields']['label']['id'] = 'label';
$handler->display->display_options['fields']['label']['table'] = 'redhen_org';
$handler->display->display_options['fields']['label']['field'] = 'label';
$handler->display->display_options['fields']['label']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['fields']['label']['label'] = 'Office';
/* Field: Message: Tank */
$handler->display->display_options['fields']['field_tank']['id'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['table'] = 'field_data_field_tank';
$handler->display->display_options['fields']['field_tank']['field'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_tank']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Gallons Removed */
$handler->display->display_options['fields']['field_gallons_removed']['id'] = 'field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['table'] = 'field_data_field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['field'] = 'field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Global: Math expression */
$handler->display->display_options['fields']['expression']['id'] = 'expression';
$handler->display->display_options['fields']['expression']['table'] = 'views';
$handler->display->display_options['fields']['expression']['field'] = 'expression';
$handler->display->display_options['fields']['expression']['label'] = 'Grams of Mercury Removed';
$handler->display->display_options['fields']['expression']['set_precision'] = TRUE;
$handler->display->display_options['fields']['expression']['precision'] = '2';
$handler->display->display_options['fields']['expression']['suffix'] = 'g';
$handler->display->display_options['fields']['expression']['expression'] = '[field_gallons_removed]*0.8';
/* Contextual filter: Organization: Plant (field_plant) */
$handler->display->display_options['arguments']['field_plant_target_id']['id'] = 'field_plant_target_id';
$handler->display->display_options['arguments']['field_plant_target_id']['table'] = 'field_data_field_plant';
$handler->display->display_options['arguments']['field_plant_target_id']['field'] = 'field_plant_target_id';
$handler->display->display_options['arguments']['field_plant_target_id']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['arguments']['field_plant_target_id']['default_action'] = 'access denied';
$handler->display->display_options['arguments']['field_plant_target_id']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['field_plant_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_plant_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_plant_target_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_plant_target_id']['validate']['type'] = 'user';
/* Filter criterion: Date: Date (message) */
$handler->display->display_options['filters']['date_filter']['id'] = 'date_filter';
$handler->display->display_options['filters']['date_filter']['table'] = 'message';
$handler->display->display_options['filters']['date_filter']['field'] = 'date_filter';
$handler->display->display_options['filters']['date_filter']['operator'] = '>=';
$handler->display->display_options['filters']['date_filter']['exposed'] = TRUE;
$handler->display->display_options['filters']['date_filter']['expose']['operator_id'] = 'date_filter_op';
$handler->display->display_options['filters']['date_filter']['expose']['label'] = 'Date Range From';
$handler->display->display_options['filters']['date_filter']['expose']['operator'] = 'date_filter_op';
$handler->display->display_options['filters']['date_filter']['expose']['identifier'] = 'date_filter';
$handler->display->display_options['filters']['date_filter']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  4 => 0,
  6 => 0,
  5 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['date_filter']['year_range'] = '2015:+0';
$handler->display->display_options['filters']['date_filter']['date_fields'] = array(
  'field_data_field_date.field_date_value' => 'field_data_field_date.field_date_value',
);
/* Filter criterion: Date: Date (message) */
$handler->display->display_options['filters']['date_filter_1']['id'] = 'date_filter_1';
$handler->display->display_options['filters']['date_filter_1']['table'] = 'message';
$handler->display->display_options['filters']['date_filter_1']['field'] = 'date_filter';
$handler->display->display_options['filters']['date_filter_1']['operator'] = '<=';
$handler->display->display_options['filters']['date_filter_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['date_filter_1']['expose']['operator_id'] = 'date_filter_1_op';
$handler->display->display_options['filters']['date_filter_1']['expose']['label'] = 'To';
$handler->display->display_options['filters']['date_filter_1']['expose']['operator'] = 'date_filter_1_op';
$handler->display->display_options['filters']['date_filter_1']['expose']['identifier'] = 'date_filter_1';
$handler->display->display_options['filters']['date_filter_1']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  4 => 0,
  6 => 0,
  5 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['date_filter_1']['year_range'] = '2015:+0';
$handler->display->display_options['filters']['date_filter_1']['date_fields'] = array(
  'field_data_field_date.field_date_value' => 'field_data_field_date.field_date_value',
);
/* Filter criterion: AQS County: County Name */
$handler->display->display_options['filters']['name']['id'] = 'name';
$handler->display->display_options['filters']['name']['table'] = 'aqs_counties';
$handler->display->display_options['filters']['name']['field'] = 'name';
$handler->display->display_options['filters']['name']['relationship'] = 'field_county_target_id';
$handler->display->display_options['filters']['name']['exposed'] = TRUE;
$handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['label'] = 'County';
$handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
$handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  4 => 0,
  5 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['name']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['name']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['name']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['name']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['name']['expose']['autocomplete_dependent'] = 0;
/* Filter criterion: Message: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'message';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'aqs_service' => 'aqs_service',
);

/* Display: Data export */
$handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'views_data_export_xls';
$handler->display->display_options['style_options']['attach_text'] = 'Download Spreadsheet';
$handler->display->display_options['style_options']['provide_file'] = 1;
$handler->display->display_options['style_options']['filename'] = '%timestamp-full.xls';
$handler->display->display_options['style_options']['parent_sort'] = 0;
$handler->display->display_options['path'] = 'compliance/export-xls';
$handler->display->display_options['displays'] = array(
  'default' => 'default',
  'page' => 0,
);

/* Display: Data export (DOC) */
$handler = $view->new_display('views_data_export', 'Data export (DOC)', 'views_data_export_2');
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['style_plugin'] = 'views_data_export_doc';
$handler->display->display_options['style_options']['attach_text'] = 'Download Word Doc';
$handler->display->display_options['style_options']['provide_file'] = 1;
$handler->display->display_options['style_options']['filename'] = '%timestamp-full.doc';
$handler->display->display_options['style_options']['parent_sort'] = 0;
$handler->display->display_options['path'] = 'compliance/export-doc';
$handler->display->display_options['displays'] = array(
  'default' => 'default',
);
