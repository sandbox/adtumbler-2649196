<?php

$view = new view();
$view->name = 'single_tank';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'aqs_tanks';
$view->human_name = 'Single Tank';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: AQSTank: Relation: Owned by office (aqs_tank → redhen_org) */
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['id'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['table'] = 'aqs_tanks';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['field'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['r_index'] = '-1';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_left'] = 0;
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_right'] = 0;
/* Relationship: Organization: Relation: Affiliated with (redhen_org → redhen_contact) */
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_contact']['id'] = 'relation_redhen_affiliation_redhen_contact';
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_contact']['table'] = 'redhen_org';
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_contact']['field'] = 'relation_redhen_affiliation_redhen_contact';
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_contact']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_contact']['r_index'] = '-1';
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_contact']['entity_deduplication_left'] = 0;
$handler->display->display_options['relationships']['relation_redhen_affiliation_redhen_contact']['entity_deduplication_right'] = 0;
/* Field: AQSTank: Tank ID */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['label'] = '';
$handler->display->display_options['fields']['id']['exclude'] = TRUE;
$handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
/* Field: AQSTank: Serial Number */
$handler->display->display_options['fields']['serial_number']['id'] = 'serial_number';
$handler->display->display_options['fields']['serial_number']['table'] = 'aqs_tanks';
$handler->display->display_options['fields']['serial_number']['field'] = 'serial_number';
/* Contextual filter: Contact: Contact ID */
$handler->display->display_options['arguments']['contact_id']['id'] = 'contact_id';
$handler->display->display_options['arguments']['contact_id']['table'] = 'redhen_contact';
$handler->display->display_options['arguments']['contact_id']['field'] = 'contact_id';
$handler->display->display_options['arguments']['contact_id']['relationship'] = 'relation_redhen_affiliation_redhen_contact';
$handler->display->display_options['arguments']['contact_id']['exception']['value'] = '';
$handler->display->display_options['arguments']['contact_id']['default_argument_type'] = 'current_user';
$handler->display->display_options['arguments']['contact_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['contact_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['contact_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['contact_id']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['contact_id']['validate']['type'] = 'numeric';
$handler->display->display_options['arguments']['contact_id']['validate']['fail'] = 'ignore';

/* Display: Entity Reference */
$handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'entityreference_style';
$handler->display->display_options['style_options']['search_fields'] = array(
  'id' => 'id',
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'entityreference_fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
