<?php
/**
 * @file
 * Template file for aqs compliance
 *
 * Available custom variables:
 * - $label: Office Name
 * - $image: photo(s) of the office
 * - $address: physical address
 * - $county: County
 * - $plant: Waste water treatment plant
 * - $phone: office phone number
 * - $users: list of users belonging to office
 * - $adduser: link to add a user
 * - $addtank: link to add a tank
 * - $tanks: list of tanks belonging to office
 */
?>

  <div class="row">
    <div class="col-sm-12">
      <div class="office-info">
        <h3><?php print render($label); ?> <span class="glyphicon glyphicon-home" aria-hidden="true"></span></h3>
        <div class="row">
          <div class="col-xs-4">
            <div class="row">
              <div class="col-sm-12">
                <?php print render($image); ?>
              </div>
            </div>
          </div>
          <div class="col-xs-4">
            <div class="row">
              <div class="col-sm-6">
                <?php print render($address); ?>
              </div>
              <div class="col-sm-6">
                <?php print render($phone); ?>
              </div>
            </div>
          </div>
          <div class="col-xs-4">
            <div class="row">
              <div class="col-sm-3">
                <?php print render($county); ?>
              </div>
              <div class="col-sm-4">
                <?php print render($operatories); ?>
              </div>
              <div class="col-sm-5">
                <?php print render($plant); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="office-users">
        <h3>Users <span class="glyphicon glyphicon-user" aria-hidden="true"></span></h3>
          <?php print render($users); ?>
      </div>
    </div>
    <div class="col-md-6">
      <div class="office-tanks">
        <h3>Tanks <span class="glyphicon glyphicon-filter" aria-hidden="true"></span></h3>
          <?php print render($tanks); ?>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="office-reports">
        <h3>Reports <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></h3>

        <ul class="nav nav-justified" role="tablist">
          <li role="presentation" class="active"><a href="#cleaner" aria-controls="cleaner" role="tab" data-toggle="tab">Cleaner Use</a></li>
          <li role="presentation"><a href="#tankinspection" aria-controls="tankinspection" role="tab" data-toggle="tab">Tank Inspection</a></li>
          <li role="presentation"><a href="#tankservice" aria-controls="tankservice" role="tab" data-toggle="tab">Tank Service</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active" id="cleaner">
            <?php print render($cleaner_report); ?>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="tankinspection">
            <?php print render($inspection_report); ?>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="tankservice">
            <?php print render($service_report); ?>
          </div>
        </div>


      </div>
    </div>
  </div>
