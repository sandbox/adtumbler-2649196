<?php

$view = new view();
$view->name = 'aqs_tank_inspection_events';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'message';
$view->human_name = 'AQS Tank Inspection Events';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Tank Inspection Events';
$handler->display->display_options['css_class'] = 'table-responsive';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['columns'] = array(
  'field_date' => 'field_date',
  'field_tank' => 'field_tank',
  'field_tank_level' => 'field_tank_level',
  'field_upper_tank_level' => 'field_upper_tank_level',
  'field_comment' => 'field_comment',
  'mid' => 'mid',
);
$handler->display->display_options['style_options']['default'] = 'field_date';
$handler->display->display_options['style_options']['info'] = array(
  'field_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_tank' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_tank_level' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_upper_tank_level' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_comment' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'mid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 'striped',
  'condensed' => 'condensed',
  'bordered' => 0,
  'hover' => 0,
);
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_tank_target_id']['id'] = 'field_tank_target_id';
$handler->display->display_options['relationships']['field_tank_target_id']['table'] = 'field_data_field_tank';
$handler->display->display_options['relationships']['field_tank_target_id']['field'] = 'field_tank_target_id';
/* Relationship: AQSTank: Relation: Owned by office (aqs_tank → redhen_org) */
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['id'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['table'] = 'aqs_tanks';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['field'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['relationship'] = 'field_tank_target_id';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['r_index'] = '-1';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_left'] = 0;
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_right'] = 0;
/* Field: Message: Time and Date */
$handler->display->display_options['fields']['field_date']['id'] = 'field_date';
$handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
$handler->display->display_options['fields']['field_date']['field'] = 'field_date';
$handler->display->display_options['fields']['field_date']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Message: Tank */
$handler->display->display_options['fields']['field_tank']['id'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['table'] = 'field_data_field_tank';
$handler->display->display_options['fields']['field_tank']['field'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Lower Tank Level */
$handler->display->display_options['fields']['field_tank_level']['id'] = 'field_tank_level';
$handler->display->display_options['fields']['field_tank_level']['table'] = 'field_data_field_tank_level';
$handler->display->display_options['fields']['field_tank_level']['field'] = 'field_tank_level';
/* Field: Message: Upper Tank Level */
$handler->display->display_options['fields']['field_upper_tank_level']['id'] = 'field_upper_tank_level';
$handler->display->display_options['fields']['field_upper_tank_level']['table'] = 'field_data_field_upper_tank_level';
$handler->display->display_options['fields']['field_upper_tank_level']['field'] = 'field_upper_tank_level';
/* Field: Message: Comment */
$handler->display->display_options['fields']['field_comment']['id'] = 'field_comment';
$handler->display->display_options['fields']['field_comment']['table'] = 'field_data_field_comment';
$handler->display->display_options['fields']['field_comment']['field'] = 'field_comment';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
$handler->display->display_options['fields']['mid']['exclude'] = TRUE;
/* Contextual filter: Organization: Organization ID */
$handler->display->display_options['arguments']['org_id']['id'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['arguments']['org_id']['field'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['arguments']['org_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['org_id']['exception']['value'] = '';
$handler->display->display_options['arguments']['org_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['org_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['org_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['org_id']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Message: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'message';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'aqs_tank_inspection' => 'aqs_tank_inspection',
);

/* Display: dental_user */
$handler = $view->new_display('embed', 'dental_user', 'dental_user');

/* Display: aqs_technician */
$handler = $view->new_display('embed', 'aqs_technician', 'aqs_technician');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['columns'] = array(
  'field_date' => 'field_date',
  'org_id' => 'org_id',
  'label' => 'label',
  'field_tank' => 'field_tank',
  'field_tank_level' => 'field_tank_level',
  'field_upper_tank_level' => 'field_upper_tank_level',
  'field_comment' => 'field_comment',
  'mid' => 'mid',
);
$handler->display->display_options['style_options']['default'] = 'field_date';
$handler->display->display_options['style_options']['info'] = array(
  'field_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'org_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'label' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_tank' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_tank_level' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_upper_tank_level' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_comment' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'mid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 'striped',
  'condensed' => 'condensed',
  'bordered' => 0,
  'hover' => 0,
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Message: Time and Date */
$handler->display->display_options['fields']['field_date']['id'] = 'field_date';
$handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
$handler->display->display_options['fields']['field_date']['field'] = 'field_date';
$handler->display->display_options['fields']['field_date']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Organization: Organization ID */
$handler->display->display_options['fields']['org_id']['id'] = 'org_id';
$handler->display->display_options['fields']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['fields']['org_id']['field'] = 'org_id';
$handler->display->display_options['fields']['org_id']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['fields']['org_id']['exclude'] = TRUE;
/* Field: Organization: Organization Name */
$handler->display->display_options['fields']['label']['id'] = 'label';
$handler->display->display_options['fields']['label']['table'] = 'redhen_org';
$handler->display->display_options['fields']['label']['field'] = 'label';
$handler->display->display_options['fields']['label']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['fields']['label']['label'] = 'Office';
$handler->display->display_options['fields']['label']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['label']['alter']['path'] = 'compliance/offices/[org_id]';
/* Field: Message: Tank */
$handler->display->display_options['fields']['field_tank']['id'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['table'] = 'field_data_field_tank';
$handler->display->display_options['fields']['field_tank']['field'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Lower Tank Level */
$handler->display->display_options['fields']['field_tank_level']['id'] = 'field_tank_level';
$handler->display->display_options['fields']['field_tank_level']['table'] = 'field_data_field_tank_level';
$handler->display->display_options['fields']['field_tank_level']['field'] = 'field_tank_level';
/* Field: Message: Upper Tank Level */
$handler->display->display_options['fields']['field_upper_tank_level']['id'] = 'field_upper_tank_level';
$handler->display->display_options['fields']['field_upper_tank_level']['table'] = 'field_data_field_upper_tank_level';
$handler->display->display_options['fields']['field_upper_tank_level']['field'] = 'field_upper_tank_level';
/* Field: Message: Comment */
$handler->display->display_options['fields']['field_comment']['id'] = 'field_comment';
$handler->display->display_options['fields']['field_comment']['table'] = 'field_data_field_comment';
$handler->display->display_options['fields']['field_comment']['field'] = 'field_comment';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
$handler->display->display_options['fields']['mid']['label'] = '';
$handler->display->display_options['fields']['mid']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['mid']['alter']['text'] = '<a href="/compliance/events/[mid]/edit" class="btn btn-link btn-sm"><span class="glyphicon glyphicon-edit"></span> edit</a>';
$handler->display->display_options['fields']['mid']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;

/* Display: by_office */
$handler = $view->new_display('embed', 'by_office', 'by_office');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Message: Time and Date */
$handler->display->display_options['fields']['field_date']['id'] = 'field_date';
$handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
$handler->display->display_options['fields']['field_date']['field'] = 'field_date';
$handler->display->display_options['fields']['field_date']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Message: Tank */
$handler->display->display_options['fields']['field_tank']['id'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['table'] = 'field_data_field_tank';
$handler->display->display_options['fields']['field_tank']['field'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Lower Tank Level */
$handler->display->display_options['fields']['field_tank_level']['id'] = 'field_tank_level';
$handler->display->display_options['fields']['field_tank_level']['table'] = 'field_data_field_tank_level';
$handler->display->display_options['fields']['field_tank_level']['field'] = 'field_tank_level';
/* Field: Message: Upper Tank Level */
$handler->display->display_options['fields']['field_upper_tank_level']['id'] = 'field_upper_tank_level';
$handler->display->display_options['fields']['field_upper_tank_level']['table'] = 'field_data_field_upper_tank_level';
$handler->display->display_options['fields']['field_upper_tank_level']['field'] = 'field_upper_tank_level';
/* Field: Message: Comment */
$handler->display->display_options['fields']['field_comment']['id'] = 'field_comment';
$handler->display->display_options['fields']['field_comment']['table'] = 'field_data_field_comment';
$handler->display->display_options['fields']['field_comment']['field'] = 'field_comment';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
$handler->display->display_options['fields']['mid']['label'] = '';
$handler->display->display_options['fields']['mid']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['mid']['alter']['text'] = '<a href="/compliance/events/[mid]/edit" class="btn btn-link btn-sm"><span class="glyphicon glyphicon-edit"></span> edit</a>';
$handler->display->display_options['fields']['mid']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Organization: Organization ID */
$handler->display->display_options['arguments']['org_id']['id'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['arguments']['org_id']['field'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['arguments']['org_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['org_id']['exception']['value'] = '';
$handler->display->display_options['arguments']['org_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['org_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['org_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['org_id']['summary_options']['items_per_page'] = '25';
