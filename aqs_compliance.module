<?php

/**
 * @file
 * Anterior Quest Compliance module
 */

module_load_include('inc', 'aqs_compliance', 'includes/aqs_compliance.forms');
module_load_include('inc', 'aqs_compliance', 'includes/aqs_compliance.views');

/**
 * Implements hook_menu()
 */
function aqs_compliance_menu() {
  $items = array();

  $items['compliance/log-service-record'] = array(
    'title' => 'Log a record',
    'page callback' => 'aqs_compliance_log_service_record',
    'access callback' => 'aqs_compliance_access',
    'access arguments' => array('log_service_event'),
    'type' => MENU_CALLBACK,
  );

  $items['compliance/log-inspection-record'] = array(
    'title' => 'Log a record',
    'page callback' => 'aqs_compliance_log_inspection_record',
    'access callback' => 'aqs_compliance_access',
    'access arguments' => array('log_inspection_event'),
    'type' => MENU_CALLBACK,
  );

  $items['compliance/log-cleaner-record'] = array(
    'title' => 'Log a record',
    'page callback' => 'aqs_compliance_log_cleaner_record',
    'access callback' => 'aqs_compliance_access',
    'access arguments' => array('log_cleaner_event'),
    'type' => MENU_CALLBACK,
  );

  $items['compliance/office/autocomplete'] = array(
    'title' => 'Autocomplete for offices',
    'page callback' => 'aqs_compliance_office_autocomplete',
    'access callback' => 'aqs_compliance_access',
    'access arguments' => array('office_autocomplete'),
    'type' => MENU_CALLBACK,
  );


  return $items;
}

/**
 * Implements hook_permissions
 */
function aqs_compliance_permission()
{
    return array(
        'access office reports' => array(
            'title' => t('Access Office Reports'),
            'description' => t('Ability to view dental office log reports'),
        ),
        'access service reports' => array(
            'title' => t('Access Service Reports'),
            'description' => t('Ability to view tank service log reports'),
        ),
        'create cleaner event' => array(
            'title' => t('Create Cleaner Events'),
            'description' => t('Ability to create a cleaner log event'),
        ),
        'create inspection event' => array(
            'title' => t('Create Tank Inspection Events'),
            'description' => t('Ability to create a tank inspection log event'),
        ),
        'create service event' => array(
            'title' => t('Create Tank Service Events'),
            'description' => t('Ability to create a tank service log event'),
        ),
        'edit aqs events' => array(
            'title' => t('Edit any event'),
            'description' => t('Ability to edit any existing log events'),
        ),
        'access compliance dashboard' => array(
            'title' => t('Access Compliance Dashboard'),
            'description' => t('Roles that can access the compliance dashboard'),
        ),
        'access aqs clients' => array(
            'title' => t('Access aqs client organizations'),
            'description' => t('Roles that can access aqs client organizations'),
        ),
        'create aqs clients' => array(
            'title' => t('Create aqs client organizations'),
            'description' => t('Roles that can create aqs client organizations'),
        ),
        'admin aqs geodata' => array(
            'title' => t('Administer AQS Counties/Plants'),
            'description' => t('Roles can add aqs counties and plants'),
        ),
    );
}

/**
 * Access callback
 */

function aqs_compliance_access($op) {

  if (!user_access('access content')){
    return FALSE;
  }

  if ($op == 'log_service_event' && user_access('create service event')) {
    return TRUE;
  }

  if ($op == 'log_cleaner_event' && user_access('create cleaner event')) {
    return TRUE;
  }

  if ($op == 'log_inspection_event' && user_access('create inspection event')) {
    return TRUE;
  }

  if ($op == 'log_weekly_record' && user_access('create cleaner event') && user_access('create inspection event')) {
    return TRUE;
  }

  if ($op == 'office_autocomplete' && user_access('access aqs clients')) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Implements hook_entity_info_alter
 */

function aqs_compliance_entity_info_alter(&$entity_info){
  // will need to do similar for URI
  $entity_info['redhen_org']['label callback'] = 'aqs_compliance_simple_label';
}

function aqs_compliance_simple_label($redhen_org) {
  return $redhen_org->label;
}

function aqs_compliance_log_inspection_record($tank = NULL) {
  global $user;
  $user_contact = redhen_contact_load_by_user($user);
  if ($user_contact && $user_contact->type == 'dental_user') {
    $related_to_user = redhen_relation_relations($user_contact);
    $related_to_user = reset($related_to_user);
    $office = $related_to_user[0];
    $query = relation_query('redhen_org', $office->org_id);
    $results = $query->propertyCondition('relation_type', 'owns_tank')->execute();
    $relations = relation_load_multiple(array_keys($results));
    $tank_ids = array();
    foreach ($relations as $relation) {
      $endpoints = reset($relation->endpoints);
      $tank_ids[] = $endpoints[1]['entity_id'];
    }
    $tanks = entity_load('aqs_tank', $tank_ids);

    return drupal_get_form('aqs_compliance_log_inspection_event_form', $tanks);
  }
  else {
    return drupal_access_denied();
  }
}

function aqs_compliance_log_cleaner_record($tank = NULL) {
  return drupal_get_form('aqs_compliance_log_cleaner_event_form');
}

function aqs_compliance_log_weekly_record() {
  global $user;
  $user_contact = redhen_contact_load_by_user($user);
  if ($user_contact && $user_contact->type == 'dental_user') {
    $related_to_user = redhen_relation_relations($user_contact);
    $related_to_user = reset($related_to_user);
    $office = $related_to_user[0];
    $query = relation_query('redhen_org', $office->org_id);
    $results = $query->propertyCondition('relation_type', 'owns_tank')->execute();
    $relations = relation_load_multiple(array_keys($results));
    $tank_ids = array();
    foreach ($relations as $relation) {
      $endpoints = reset($relation->endpoints);
      $tank_ids[] = $endpoints[1]['entity_id'];
    }
    $tanks = entity_load('aqs_tank', $tank_ids);

    return drupal_get_form('aqs_compliance_log_weekly_record_form', $office, $tanks);
  }
  else {
    return drupal_access_denied();
  }
}

/**
 * Offices autocomplete callback
 */
function aqs_compliance_office_autocomplete($input) {
  $query = new EntityFieldQuery;

  $query->entityCondition('entity_type', 'redhen_org');
  $query->propertyCondition('label', $input, 'CONTAINS');
  $query->range(0, 10);

  $results = $query->execute();
  $options = array();
  if ($results) {
    $offices = entity_load('redhen_org', array_keys(reset($results)));

    foreach ($offices as $office) {
      $entity_id = $office->internalIdentifier();
      $entity_label = $office->label();
      $options[$entity_label . ' (' . $entity_id . ')'] = check_plain($entity_label);
    }
  }

  drupal_json_output($options);
}

/**
 * Implements hook_views_api().
 */
function aqs_compliance_views_api() {
  return array(
    'api' => 3,
    'path'=> drupal_get_path('module', 'aqs_compliance') . '/includes',
  );
}
