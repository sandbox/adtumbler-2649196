<?php
/**
 * @file
 * Template file for aqs compliance
 *
 * Available custom variables:
 * -
 */
?>
  <!-- AQS Compliance -->

  <div class="compliance-container row">
    <div class="col-xs-4">
      <a class="section" href="/compliance/office"><span class="glyphicon glyphicon-home">&nbsp;</span>
        <h4>Office</h4>
      </a>
    </div>

    <div class="col-xs-4">
      <a aria-controls="log" class="section" data-toggle="tab" href="#log" role="tab"><span class="glyphicon glyphicon-pencil">&nbsp;</span>

        <h4>Log Record</h4>
      </a>
    </div>

    <div class="col-xs-4">
      <a aria-controls="education" class="section" data-toggle="tab" href="#education" role="tab"><span class="glyphicon glyphicon-education">&nbsp;</span>

        <h4>Education</h4>
      </a>
    </div>
  </div>

  <div>
    <!-- Tab panes -->
    <div class="tab-content dialog-tabs">
      <div class="tab-pane fade" id="log" role="tabpanel">
        <?php print render($log_inspection_event); ?>
        <?php print render($log_cleaner_event); ?>
      </div>

      <div class="tab-pane fade" id="education" role="tabpanel"><?php print render($courses); ?></div>
    </div>
  </div>
