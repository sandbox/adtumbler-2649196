<?php

$view = new view();
$view->name = 'aqs_counties';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'aqs_counties';
$view->human_name = 'AQS Counties';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['columns'] = array(
  'id' => 'id',
  'name' => 'name',
  'field_state' => 'field_state',
);
$handler->display->display_options['style_options']['default'] = 'name';
$handler->display->display_options['style_options']['info'] = array(
  'id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_state' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 'striped',
  'condensed' => 'condensed',
  'bordered' => 0,
  'hover' => 0,
);
/* Field: AQS County: County ID */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'aqs_counties';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['exclude'] = TRUE;
/* Field: AQS County: County Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'aqs_counties';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = 'County';
/* Field: AQS County: State */
$handler->display->display_options['fields']['field_state']['id'] = 'field_state';
$handler->display->display_options['fields']['field_state']['table'] = 'field_data_field_state';
$handler->display->display_options['fields']['field_state']['field'] = 'field_state';
/* Filter criterion: AQS County: State (field_state) */
$handler->display->display_options['filters']['field_state_value']['id'] = 'field_state_value';
$handler->display->display_options['filters']['field_state_value']['table'] = 'field_data_field_state';
$handler->display->display_options['filters']['field_state_value']['field'] = 'field_state_value';
$handler->display->display_options['filters']['field_state_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_state_value']['expose']['operator_id'] = 'field_state_value_op';
$handler->display->display_options['filters']['field_state_value']['expose']['label'] = 'State';
$handler->display->display_options['filters']['field_state_value']['expose']['operator'] = 'field_state_value_op';
$handler->display->display_options['filters']['field_state_value']['expose']['identifier'] = 'field_state_value';
$handler->display->display_options['filters']['field_state_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  4 => 0,
  6 => 0,
  5 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['field_state_value']['group_info']['label'] = 'State';
$handler->display->display_options['filters']['field_state_value']['group_info']['identifier'] = 'field_state_value';
$handler->display->display_options['filters']['field_state_value']['group_info']['group_items'] = array(
  1 => array(
    'title' => '',
    'operator' => 'or',
    'value' => array(),
  ),
);
