<?php
/**
 * @file
 * Template file for aqs compliance
 *
 * Available custom variables:
 * -
 */
?>
  <!-- AQS Compliance -->

<div class="tech-box"><h3>Select Office</h3>
  <?php print render($office_quick_search); ?>
<ul class="nav nav-justified" role="tablist">
          <li role="presentation" class="active"><a href="#cleaner" aria-controls="cleaner" role="tab" data-toggle="tab" class="btn btn-primary btn-lg disabled"><span class="glyphicon glyphicon-screenshot"></span> Near My Location</a></li>
          <li role="presentation"><a href="#tankservice" aria-controls="tankservice" role="tab" data-toggle="tab" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-map-marker"></span> By City</a></li>
          <li role="presentation"><a href="#tankinspection" aria-controls="tankinspection" role="tab" data-toggle="tab" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-list"></span> By Office Name</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade" id="cleaner">
          </div>
          <div role="tabpanel" class="tab-pane fade" id="tankservice">
            <?php print render($by_city); ?>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="tankinspection">
            <?php print render($by_office); ?>
          </div>
        </div>
</div>
