<?php

$view = new view();
$view->name = 'aqs_tank_service_events';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'message';
$view->human_name = 'AQS Tank Service Events';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Tank Service Events';
$handler->display->display_options['css_class'] = 'table-responsive';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['columns'] = array(
  'field_date' => 'field_date',
  'field_tank' => 'field_tank',
  'field_tech' => 'field_tech',
  'field_gallons_removed' => 'field_gallons_removed',
  'field_comment' => 'field_comment',
  'mid' => 'mid',
);
$handler->display->display_options['style_options']['default'] = 'field_date';
$handler->display->display_options['style_options']['info'] = array(
  'field_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_tank' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_tech' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_gallons_removed' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_comment' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'mid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 'striped',
  'condensed' => 'condensed',
  'bordered' => 0,
  'hover' => 0,
);
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'No events found';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_tank_target_id']['id'] = 'field_tank_target_id';
$handler->display->display_options['relationships']['field_tank_target_id']['table'] = 'field_data_field_tank';
$handler->display->display_options['relationships']['field_tank_target_id']['field'] = 'field_tank_target_id';
/* Relationship: AQSTank: Relation: Owned by office (aqs_tank → redhen_org) */
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['id'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['table'] = 'aqs_tanks';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['field'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['relationship'] = 'field_tank_target_id';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['r_index'] = '-1';
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_left'] = 0;
$handler->display->display_options['relationships']['relation_owns_tank_redhen_org']['entity_deduplication_right'] = 0;
/* Field: Message: Time and Date */
$handler->display->display_options['fields']['field_date']['id'] = 'field_date';
$handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
$handler->display->display_options['fields']['field_date']['field'] = 'field_date';
$handler->display->display_options['fields']['field_date']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Message: Tank */
$handler->display->display_options['fields']['field_tank']['id'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['table'] = 'field_data_field_tank';
$handler->display->display_options['fields']['field_tank']['field'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Tech */
$handler->display->display_options['fields']['field_tech']['id'] = 'field_tech';
$handler->display->display_options['fields']['field_tech']['table'] = 'field_data_field_tech';
$handler->display->display_options['fields']['field_tech']['field'] = 'field_tech';
$handler->display->display_options['fields']['field_tech']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Gallons Removed */
$handler->display->display_options['fields']['field_gallons_removed']['id'] = 'field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['table'] = 'field_data_field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['field'] = 'field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Message: Comment */
$handler->display->display_options['fields']['field_comment']['id'] = 'field_comment';
$handler->display->display_options['fields']['field_comment']['table'] = 'field_data_field_comment';
$handler->display->display_options['fields']['field_comment']['field'] = 'field_comment';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
/* Contextual filter: Organization: Organization ID */
$handler->display->display_options['arguments']['org_id']['id'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['arguments']['org_id']['field'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['arguments']['org_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['org_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['org_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['org_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['org_id']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Message: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'message';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'aqs_service' => 'aqs_service',
);

/* Display: dental_user */
$handler = $view->new_display('embed', 'dental_user', 'dental_user');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'views_aggregator';
$handler->display->display_options['style_options']['columns'] = array(
  'field_date' => 'field_date',
  'field_tank' => 'field_tank',
  'field_tech' => 'field_tech',
  'field_gallons_removed' => 'field_gallons_removed',
  'expression' => 'expression',
  'field_comment' => 'field_comment',
  'mid' => 'mid',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'field_date' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'field_tank' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'field_tech' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'field_gallons_removed' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 1,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => 'Total',
  ),
  'expression' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 1,
    'aggr_column' => 'views_aggregator_expression',
    'aggr_par_column' => '[field_gallons_removed]*0.8',
  ),
  'field_comment' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'mid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
);
$handler->display->display_options['style_options']['column_aggregation']['totals_per_page'] = '0';
$handler->display->display_options['style_options']['column_aggregation']['totals_row_position'] = array(
  1 => '1',
  2 => 0,
);
$handler->display->display_options['style_options']['column_aggregation']['precision'] = '2';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Message: Time and Date */
$handler->display->display_options['fields']['field_date']['id'] = 'field_date';
$handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
$handler->display->display_options['fields']['field_date']['field'] = 'field_date';
$handler->display->display_options['fields']['field_date']['settings'] = array(
  'format_type' => 'date_only',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Message: Tank */
$handler->display->display_options['fields']['field_tank']['id'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['table'] = 'field_data_field_tank';
$handler->display->display_options['fields']['field_tank']['field'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Tech */
$handler->display->display_options['fields']['field_tech']['id'] = 'field_tech';
$handler->display->display_options['fields']['field_tech']['table'] = 'field_data_field_tech';
$handler->display->display_options['fields']['field_tech']['field'] = 'field_tech';
$handler->display->display_options['fields']['field_tech']['label'] = '';
$handler->display->display_options['fields']['field_tech']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_tech']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_tech']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Gallons Removed */
$handler->display->display_options['fields']['field_gallons_removed']['id'] = 'field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['table'] = 'field_data_field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['field'] = 'field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Global: Math expression */
$handler->display->display_options['fields']['expression']['id'] = 'expression';
$handler->display->display_options['fields']['expression']['table'] = 'views';
$handler->display->display_options['fields']['expression']['field'] = 'expression';
$handler->display->display_options['fields']['expression']['label'] = 'Mercury Removed';
$handler->display->display_options['fields']['expression']['set_precision'] = TRUE;
$handler->display->display_options['fields']['expression']['precision'] = '1';
$handler->display->display_options['fields']['expression']['separator'] = '';
$handler->display->display_options['fields']['expression']['suffix'] = 'g';
$handler->display->display_options['fields']['expression']['expression'] = '0.8*[field_gallons_removed]';
/* Field: Message: Comment */
$handler->display->display_options['fields']['field_comment']['id'] = 'field_comment';
$handler->display->display_options['fields']['field_comment']['table'] = 'field_data_field_comment';
$handler->display->display_options['fields']['field_comment']['field'] = 'field_comment';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
$handler->display->display_options['fields']['mid']['label'] = '';
$handler->display->display_options['fields']['mid']['exclude'] = TRUE;
$handler->display->display_options['fields']['mid']['element_label_colon'] = FALSE;

/* Display: aqs_technician */
$handler = $view->new_display('embed', 'aqs_technician', 'aqs_technician');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
$handler->display->display_options['style_options']['columns'] = array(
  'field_date' => 'field_date',
  'org_id' => 'org_id',
  'label' => 'label',
  'field_tank' => 'field_tank',
  'field_tech' => 'field_tech',
  'field_gallons_removed' => 'field_gallons_removed',
  'field_comment' => 'field_comment',
  'mid' => 'mid',
);
$handler->display->display_options['style_options']['default'] = 'field_date';
$handler->display->display_options['style_options']['info'] = array(
  'field_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'org_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'label' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_tank' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_tech' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_gallons_removed' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_comment' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'mid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['bootstrap_styles'] = array(
  'striped' => 0,
  'bordered' => 0,
  'hover' => 0,
  'condensed' => 0,
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Message: Time and Date */
$handler->display->display_options['fields']['field_date']['id'] = 'field_date';
$handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
$handler->display->display_options['fields']['field_date']['field'] = 'field_date';
$handler->display->display_options['fields']['field_date']['settings'] = array(
  'format_type' => 'date_only',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Organization: Organization ID */
$handler->display->display_options['fields']['org_id']['id'] = 'org_id';
$handler->display->display_options['fields']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['fields']['org_id']['field'] = 'org_id';
$handler->display->display_options['fields']['org_id']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['fields']['org_id']['label'] = '';
$handler->display->display_options['fields']['org_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['org_id']['element_label_colon'] = FALSE;
/* Field: Organization: Organization Name */
$handler->display->display_options['fields']['label']['id'] = 'label';
$handler->display->display_options['fields']['label']['table'] = 'redhen_org';
$handler->display->display_options['fields']['label']['field'] = 'label';
$handler->display->display_options['fields']['label']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['fields']['label']['label'] = 'Office';
$handler->display->display_options['fields']['label']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['label']['alter']['path'] = 'compliance/offices/[org_id]';
/* Field: Message: Tank */
$handler->display->display_options['fields']['field_tank']['id'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['table'] = 'field_data_field_tank';
$handler->display->display_options['fields']['field_tank']['field'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Tech */
$handler->display->display_options['fields']['field_tech']['id'] = 'field_tech';
$handler->display->display_options['fields']['field_tech']['table'] = 'field_data_field_tech';
$handler->display->display_options['fields']['field_tech']['field'] = 'field_tech';
$handler->display->display_options['fields']['field_tech']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Gallons Removed */
$handler->display->display_options['fields']['field_gallons_removed']['id'] = 'field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['table'] = 'field_data_field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['field'] = 'field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Message: Comment */
$handler->display->display_options['fields']['field_comment']['id'] = 'field_comment';
$handler->display->display_options['fields']['field_comment']['table'] = 'field_data_field_comment';
$handler->display->display_options['fields']['field_comment']['field'] = 'field_comment';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
$handler->display->display_options['fields']['mid']['label'] = '';
$handler->display->display_options['fields']['mid']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['mid']['alter']['text'] = '<a href="/compliance/events/[mid]/edit" class="btn btn-link btn-sm"><span class="glyphicon glyphicon-edit"></span> edit</a>';
$handler->display->display_options['fields']['mid']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;

/* Display: by_office */
$handler = $view->new_display('embed', 'by_office', 'by_office');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'views_aggregator';
$handler->display->display_options['style_options']['columns'] = array(
  'field_date' => 'field_date',
  'field_tank' => 'field_tank',
  'field_tech' => 'field_tech',
  'field_gallons_removed' => 'field_gallons_removed',
  'expression' => 'expression',
  'field_comment' => 'field_comment',
  'mid' => 'mid',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'field_date' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'field_tank' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'field_tech' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'field_gallons_removed' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 1,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => 'Total',
  ),
  'expression' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 1,
    'aggr_column' => 'views_aggregator_expression',
    'aggr_par_column' => '[field_gallons_removed]*0.8',
  ),
  'field_comment' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
  'mid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_aggr' => 0,
    'aggr' => array(
      'views_aggregator_first' => 'views_aggregator_first',
    ),
    'aggr_par' => '',
    'has_aggr_column' => 0,
    'aggr_column' => 'views_aggregator_sum',
    'aggr_par_column' => '',
  ),
);
$handler->display->display_options['style_options']['column_aggregation']['totals_per_page'] = '0';
$handler->display->display_options['style_options']['column_aggregation']['totals_row_position'] = array(
  1 => '1',
  2 => 0,
);
$handler->display->display_options['style_options']['column_aggregation']['precision'] = '2';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Message: Time and Date */
$handler->display->display_options['fields']['field_date']['id'] = 'field_date';
$handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
$handler->display->display_options['fields']['field_date']['field'] = 'field_date';
$handler->display->display_options['fields']['field_date']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Message: Tank */
$handler->display->display_options['fields']['field_tank']['id'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['table'] = 'field_data_field_tank';
$handler->display->display_options['fields']['field_tank']['field'] = 'field_tank';
$handler->display->display_options['fields']['field_tank']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Tech */
$handler->display->display_options['fields']['field_tech']['id'] = 'field_tech';
$handler->display->display_options['fields']['field_tech']['table'] = 'field_data_field_tech';
$handler->display->display_options['fields']['field_tech']['field'] = 'field_tech';
$handler->display->display_options['fields']['field_tech']['settings'] = array(
  'link' => 0,
);
/* Field: Message: Gallons Removed */
$handler->display->display_options['fields']['field_gallons_removed']['id'] = 'field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['table'] = 'field_data_field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['field'] = 'field_gallons_removed';
$handler->display->display_options['fields']['field_gallons_removed']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Global: Math expression */
$handler->display->display_options['fields']['expression']['id'] = 'expression';
$handler->display->display_options['fields']['expression']['table'] = 'views';
$handler->display->display_options['fields']['expression']['field'] = 'expression';
$handler->display->display_options['fields']['expression']['set_precision'] = TRUE;
$handler->display->display_options['fields']['expression']['precision'] = '1';
$handler->display->display_options['fields']['expression']['separator'] = '';
$handler->display->display_options['fields']['expression']['suffix'] = 'g';
$handler->display->display_options['fields']['expression']['expression'] = '[field_gallons_removed]*0.8';
/* Field: Message: Comment */
$handler->display->display_options['fields']['field_comment']['id'] = 'field_comment';
$handler->display->display_options['fields']['field_comment']['table'] = 'field_data_field_comment';
$handler->display->display_options['fields']['field_comment']['field'] = 'field_comment';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
$handler->display->display_options['fields']['mid']['label'] = '';
$handler->display->display_options['fields']['mid']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['mid']['alter']['text'] = '<a href="/compliance/events/[mid]/edit" class="btn btn-link btn-sm"><span class="glyphicon glyphicon-edit"></span> edit</a>';
$handler->display->display_options['fields']['mid']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Organization: Organization ID */
$handler->display->display_options['arguments']['org_id']['id'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['table'] = 'redhen_org';
$handler->display->display_options['arguments']['org_id']['field'] = 'org_id';
$handler->display->display_options['arguments']['org_id']['relationship'] = 'relation_owns_tank_redhen_org';
$handler->display->display_options['arguments']['org_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['org_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['org_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['org_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['org_id']['summary_options']['items_per_page'] = '25';

